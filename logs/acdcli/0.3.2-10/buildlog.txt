+ date
Sun Apr 16 17:51:36 UTC 2023
+ apt-get source --only-source acdcli=0.3.2-10
Reading package lists...
Need to get 54.5 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main acdcli 0.3.2-10 (dsc) [1509 B]
Get:2 http://deb.devuan.org/merged chimaera/main acdcli 0.3.2-10 (tar) [51.5 kB]
Get:3 http://deb.devuan.org/merged chimaera/main acdcli 0.3.2-10 (diff) [1480 B]
dpkg-source: info: extracting acdcli in acdcli-0.3.2
dpkg-source: info: unpacking acdcli_0.3.2.orig.tar.gz
dpkg-source: info: unpacking acdcli_0.3.2-10.debian.tar.xz
Fetched 54.5 kB in 1s (41.5 kB/s)
W: Download is performed unsandboxed as root as file 'acdcli_0.3.2-10.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source acdcli=0.3.2-10
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-python libfuse-dev libselinux1-dev libsepol1-dev python3-all
  python3-appdirs python3-colorama python3-fusepy python3-requests-toolbelt
  python3-setuptools
0 upgraded, 10 newly installed, 0 to remove and 0 not upgraded.
Need to get 2099 kB of archives.
After this operation, 11.7 MB of additional disk space will be used.
Get:1 http://deb.devuan.org/merged chimaera/main amd64 dh-python all 4.20201102+nmu1 [99.4 kB]
Get:2 http://deb.devuan.org/merged chimaera/main amd64 libsepol1-dev amd64 3.1-1 [338 kB]
Get:3 http://deb.devuan.org/merged chimaera/main amd64 libselinux1-dev amd64 3.1-3 [168 kB]
Get:4 http://deb.devuan.org/merged chimaera/main amd64 libfuse-dev amd64 2.9.9-5 [1031 kB]
Get:5 http://deb.devuan.org/merged chimaera/main amd64 python3-all amd64 3.9.2-3 [1056 B]
Get:6 http://deb.devuan.org/merged chimaera/main amd64 python3-appdirs all 1.4.4-1 [12.7 kB]
Get:7 http://deb.devuan.org/merged chimaera/main amd64 python3-colorama all 0.4.4-1 [28.5 kB]
Get:8 http://deb.devuan.org/merged chimaera/main amd64 python3-fusepy all 3.0.1-2 [11.2 kB]
Get:9 http://deb.devuan.org/merged chimaera/main amd64 python3-requests-toolbelt all 0.9.1-1 [41.7 kB]
Get:10 http://deb.devuan.org/merged chimaera/main amd64 python3-setuptools all 52.0.0-4 [366 kB]
Fetched 2099 kB in 0s (11.3 MB/s)
Selecting previously unselected package dh-python.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 99471 files and directories currently installed.)
Preparing to unpack .../0-dh-python_4.20201102+nmu1_all.deb ...
Unpacking dh-python (4.20201102+nmu1) ...
Selecting previously unselected package libsepol1-dev:amd64.
Preparing to unpack .../1-libsepol1-dev_3.1-1_amd64.deb ...
Unpacking libsepol1-dev:amd64 (3.1-1) ...
Selecting previously unselected package libselinux1-dev:amd64.
Preparing to unpack .../2-libselinux1-dev_3.1-3_amd64.deb ...
Unpacking libselinux1-dev:amd64 (3.1-3) ...
Selecting previously unselected package libfuse-dev:amd64.
Preparing to unpack .../3-libfuse-dev_2.9.9-5_amd64.deb ...
Unpacking libfuse-dev:amd64 (2.9.9-5) ...
Selecting previously unselected package python3-all.
Preparing to unpack .../4-python3-all_3.9.2-3_amd64.deb ...
Unpacking python3-all (3.9.2-3) ...
Selecting previously unselected package python3-appdirs.
Preparing to unpack .../5-python3-appdirs_1.4.4-1_all.deb ...
Unpacking python3-appdirs (1.4.4-1) ...
Selecting previously unselected package python3-colorama.
Preparing to unpack .../6-python3-colorama_0.4.4-1_all.deb ...
Unpacking python3-colorama (0.4.4-1) ...
Selecting previously unselected package python3-fusepy.
Preparing to unpack .../7-python3-fusepy_3.0.1-2_all.deb ...
Unpacking python3-fusepy (3.0.1-2) ...
Selecting previously unselected package python3-requests-toolbelt.
Preparing to unpack .../8-python3-requests-toolbelt_0.9.1-1_all.deb ...
Unpacking python3-requests-toolbelt (0.9.1-1) ...
Selecting previously unselected package python3-setuptools.
Preparing to unpack .../9-python3-setuptools_52.0.0-4_all.deb ...
Unpacking python3-setuptools (52.0.0-4) ...
Setting up dh-python (4.20201102+nmu1) ...
Setting up python3-requests-toolbelt (0.9.1-1) ...
Setting up python3-setuptools (52.0.0-4) ...
Setting up python3-colorama (0.4.4-1) ...
Setting up libsepol1-dev:amd64 (3.1-1) ...
Setting up python3-all (3.9.2-3) ...
Setting up python3-fusepy (3.0.1-2) ...
Setting up python3-appdirs (1.4.4-1) ...
Setting up libselinux1-dev:amd64 (3.1-3) ...
Setting up libfuse-dev:amd64 (2.9.9-5) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name acdcli* -type d
+ cd ./acdcli-0.3.2
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package acdcli
dpkg-buildpackage: info: source version 0.3.2-10
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Franco (nextime) Lanza <nextime@devuan.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --with python3 --buildsystem=pybuild
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_clean -O--buildsystem=pybuild
dh_auto_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
I: pybuild base:232: python3.9 setup.py clean 
running clean
removing '/build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build' (and everything under it)
'build/bdist.linux-x86_64' does not exist -- can't clean it
'build/scripts-3.9' does not exist -- can't clean it
   dh_clean -O--buildsystem=pybuild
dh_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building acdcli using existing ./acdcli_0.3.2.orig.tar.gz
dpkg-source: info: building acdcli in acdcli_0.3.2-10.debian.tar.xz
dpkg-source: info: building acdcli in acdcli_0.3.2-10.dsc
 debian/rules build
dh build --with python3 --buildsystem=pybuild
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_update_autotools_config -O--buildsystem=pybuild
   dh_auto_configure -O--buildsystem=pybuild
dh_auto_configure: warning: Compatibility levels before 10 are deprecated (level 9 in use)
I: pybuild base:232: python3.9 setup.py config 
running config
   dh_auto_build -O--buildsystem=pybuild
dh_auto_build: warning: Compatibility levels before 10 are deprecated (level 9 in use)
I: pybuild base:232: /usr/bin/python3 setup.py build 
running build
running build_py
creating /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli
copying acdcli/acd_fuse.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli
copying acdcli/__init__.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli
creating /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/cache
copying acdcli/cache/cursors.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/cache
copying acdcli/cache/__init__.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/cache
copying acdcli/cache/schema.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/cache
copying acdcli/cache/format.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/cache
copying acdcli/cache/db.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/cache
copying acdcli/cache/sync.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/cache
copying acdcli/cache/query.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/cache
creating /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/plugins
copying acdcli/plugins/template.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/plugins
copying acdcli/plugins/__init__.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/plugins
creating /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/api
copying acdcli/api/metadata.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/api
copying acdcli/api/common.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/api
copying acdcli/api/content.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/api
copying acdcli/api/client.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/api
copying acdcli/api/trash.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/api
copying acdcli/api/__init__.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/api
copying acdcli/api/backoff_req.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/api
copying acdcli/api/account.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/api
copying acdcli/api/oauth.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/api
creating /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/utils
copying acdcli/utils/time.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/utils
copying acdcli/utils/hashing.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/utils
copying acdcli/utils/__init__.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/utils
copying acdcli/utils/progress.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/utils
copying acdcli/utils/conf.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/utils
copying acdcli/utils/threading.py -> /build/acdcli/acdcli-0.3.2/.pybuild/cpython3_3.9_acdcli/build/acdcli/utils
running build_scripts
creating build
creating build/scripts-3.9
copying and adjusting acd_cli.py -> build/scripts-3.9
changing mode of build/scripts-3.9/acd_cli.py from 644 to 755
   dh_auto_test -O--buildsystem=pybuild
dh_auto_test: warning: Compatibility levels before 10 are deprecated (level 9 in use)
I: pybuild base:232: python3.9 setup.py test 
running test
WARNING: Testing via this command is deprecated and will be removed in a future version. Users looking for a generic test entry point independent of test runner are encouraged to use tox.
running egg_info
writing acdcli.egg-info/PKG-INFO
writing dependency_links to acdcli.egg-info/dependency_links.txt
writing entry points to acdcli.egg-info/entry_points.txt
writing requirements to acdcli.egg-info/requires.txt
writing top-level names to acdcli.egg-info/top_level.txt
reading manifest file 'acdcli.egg-info/SOURCES.txt'
writing manifest file 'acdcli.egg-info/SOURCES.txt'
running build_ext
acd_fuse (unittest.loader._FailedTest) ... ERROR

======================================================================
ERROR: acd_fuse (unittest.loader._FailedTest)
----------------------------------------------------------------------
ImportError: Failed to import test module: acd_fuse
Traceback (most recent call last):
  File "/usr/lib/python3.9/unittest/loader.py", line 154, in loadTestsFromName
    module = __import__(module_name)
  File "/build/acdcli/acdcli-0.3.2/acdcli/acd_fuse.py", line 29, in <module>
    from fuse import FUSE, FuseOSError as FuseError, Operations
ModuleNotFoundError: No module named 'fuse'


----------------------------------------------------------------------
Ran 1 test in 0.000s

FAILED (errors=1)
Test failed: <unittest.runner.TextTestResult run=1 errors=1 failures=0>
error: Test failed: <unittest.runner.TextTestResult run=1 errors=1 failures=0>
E: pybuild pybuild:353: test: plugin distutils failed with: exit code=1: python3.9 setup.py test 
dh_auto_test: error: pybuild --test -i python{version} -p 3.9 returned exit code 13
make: *** [debian/rules:7: build] Error 25
dpkg-buildpackage: error: debian/rules build subprocess returned exit status 2
