+ date
Sun Apr 16 20:12:51 UTC 2023
+ apt-get source --only-source jenkins-buildenv-devuan=1.2
Reading package lists...
NOTICE: 'jenkins-buildenv-devuan' packaging is maintained in the 'Git' version control system at:
https://git.devuan.org/devuan/jenkins-buildenv-devuan.git
Please use:
git clone https://git.devuan.org/devuan/jenkins-buildenv-devuan.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 4512 B of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main jenkins-buildenv-devuan 1.2 (dsc) [1284 B]
Get:2 http://deb.devuan.org/merged chimaera/main jenkins-buildenv-devuan 1.2 (tar) [3228 B]
dpkg-source: info: extracting jenkins-buildenv-devuan in jenkins-buildenv-devuan-1.2
dpkg-source: info: unpacking jenkins-buildenv-devuan_1.2.tar.xz
Fetched 4512 B in 0s (82.7 kB/s)
W: Download is performed unsandboxed as root as file 'jenkins-buildenv-devuan_1.2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source jenkins-buildenv-devuan=1.2
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name jenkins-buildenv-devuan* -type d
+ cd ./jenkins-buildenv-devuan-1.2
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package jenkins-buildenv-devuan
dpkg-buildpackage: info: source version 1.2
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Mark Hindley <mark@hindley.org.uk>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building jenkins-buildenv-devuan in jenkins-buildenv-devuan_1.2.tar.xz
dpkg-source: info: building jenkins-buildenv-devuan in jenkins-buildenv-devuan_1.2.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'jenkins-buildenv-devuan' in '../jenkins-buildenv-devuan_1.2_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../jenkins-buildenv-devuan_1.2_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Apr 16 20:12:57 UTC 2023
+ cd ..
+ ls -la
total 40
drwxr-xr-x 3 root root 4096 Apr 16 20:12 .
drwxr-xr-x 3 root root 4096 Apr 16 20:12 ..
-rw-r--r-- 1 root root 2819 Apr 16 20:12 buildlog.txt
drwxr-xr-x 4 root root 4096 Nov 14 11:32 jenkins-buildenv-devuan-1.2
-rw-r--r-- 1 root root  746 Apr 16 20:12 jenkins-buildenv-devuan_1.2.dsc
-rw-r--r-- 1 root root 3220 Apr 16 20:12 jenkins-buildenv-devuan_1.2.tar.xz
-rw-r--r-- 1 root root 2392 Apr 16 20:12 jenkins-buildenv-devuan_1.2_all.deb
-rw-r--r-- 1 root root 5124 Apr 16 20:12 jenkins-buildenv-devuan_1.2_amd64.buildinfo
-rw-r--r-- 1 root root 1645 Apr 16 20:12 jenkins-buildenv-devuan_1.2_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./jenkins-buildenv-devuan_1.2_amd64.buildinfo ./buildlog.txt ./jenkins-buildenv-devuan_1.2.tar.xz ./jenkins-buildenv-devuan_1.2.dsc ./jenkins-buildenv-devuan_1.2_amd64.changes ./jenkins-buildenv-devuan_1.2_all.deb
4b6cdc1eddae5bdd2d2ece7dbcf48e3297f92f73081b3afd0797a82cf48a5ed2  ./jenkins-buildenv-devuan_1.2_amd64.buildinfo
17a1450759d58ec8fd1cc8f768ec301630b0c77aaa9be70f735e807fc7e9eef0  ./buildlog.txt
ef801a1bb81aa944cf6ebbf3987a14ff29ec607660e4fb1b8c50e841ac703ce2  ./jenkins-buildenv-devuan_1.2.tar.xz
17fdae020691f6d036a4ea6b81f6920495e53cb06cf1b83d8e6b7922d683bf44  ./jenkins-buildenv-devuan_1.2.dsc
ef5d2b3b02345fa1f3c9d07db87231cd1f2886c851acb83565fe97d6763160a7  ./jenkins-buildenv-devuan_1.2_amd64.changes
1f7b9d74308af410e73c1b54293ea19a48e34f14e12cda8e1e1591ce1861d579  ./jenkins-buildenv-devuan_1.2_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls jenkins-buildenv-devuan_1.2_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/j/jenkins-buildenv-devuan/jenkins-buildenv-devuan_1.2_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./jenkins-buildenv-devuan_1.2_all.deb
1f7b9d74308af410e73c1b54293ea19a48e34f14e12cda8e1e1591ce1861d579  ./jenkins-buildenv-devuan_1.2_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./jenkins-buildenv-devuan_1.2_all.deb: OK
+ echo Package jenkins-buildenv-devuan version 1.2 is reproducible!
Package jenkins-buildenv-devuan version 1.2 is reproducible!
