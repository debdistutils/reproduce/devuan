+ date
Sun Apr 16 19:47:45 UTC 2023
+ apt-get source --only-source devuan-keyring=2022.09.04
Reading package lists...
Need to get 54.4 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main devuan-keyring 2022.09.04 (dsc) [1340 B]
Get:2 http://deb.devuan.org/merged chimaera/main devuan-keyring 2022.09.04 (tar) [53.0 kB]
dpkg-source: info: extracting devuan-keyring in devuan-keyring-2022.09.04
dpkg-source: info: unpacking devuan-keyring_2022.09.04.tar.xz
Fetched 54.4 kB in 0s (284 kB/s)
W: Download is performed unsandboxed as root as file 'devuan-keyring_2022.09.04.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source devuan-keyring=2022.09.04
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name devuan-keyring* -type d
+ cd ./devuan-keyring-2022.09.04
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package devuan-keyring
dpkg-buildpackage: info: source version 2022.09.04
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Ralph Ronnquist <ralph.ronnquist@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
test -f keyrings/devuan-keyring.gpg
rm -f foo foo.asc *.bak *~ */*~ debian/files* debian/*substvars
rm -rf debian/tmp debian/devuan-keyring-udeb
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building devuan-keyring in devuan-keyring_2022.09.04.tar.xz
dpkg-source: info: building devuan-keyring in devuan-keyring_2022.09.04.dsc
 debian/rules build
make: Nothing to be done for 'build'.
 debian/rules binary
test -f keyrings/devuan-keyring.gpg
test root = "`whoami`"
test -f keyrings/devuan-keyring.gpg
rm -rf debian/tmp
install -d -m 755 debian/tmp/DEBIAN/
install -m 755 debian/postinst debian/tmp/DEBIAN/
install -d -m 755 debian/tmp/usr/share/keyrings/
install -m 644 keyrings/devuan-keyring.gpg debian/tmp/usr/share/keyrings/
install -m 644 keyrings/devuan-archive-keyring.gpg debian/tmp/usr/share/keyrings/
install -d -m 755 debian/tmp/etc/apt/trusted.gpg.d/
install -m 644 keyrings/devuan-keyring-2016-archive.gpg debian/tmp/etc/apt/trusted.gpg.d/
#install -m 644 keyrings/devuan-keyring-2017-archive.gpg debian/tmp/etc/apt/trusted.gpg.d/
install -m 644 keyrings/devuan-keyring-2022-archive.gpg debian/tmp/etc/apt/trusted.gpg.d/
install -m 644 keyrings/devuan-keyring-2016-cdimage.gpg debian/tmp/etc/apt/trusted.gpg.d/
install -d -m 755 debian/tmp/usr/share/doc/devuan-keyring/
install -m 644 README.md debian/tmp/usr/share/doc/devuan-keyring/
install -m 644 debian/changelog debian/tmp/usr/share/doc/devuan-keyring/changelog
gzip -9v debian/tmp/usr/share/doc/devuan-keyring/*
debian/tmp/usr/share/doc/devuan-keyring/README.md:	 25.7% -- replaced with debian/tmp/usr/share/doc/devuan-keyring/README.md.gz
debian/tmp/usr/share/doc/devuan-keyring/changelog:	 70.5% -- replaced with debian/tmp/usr/share/doc/devuan-keyring/changelog.gz
install -m 644 debian/copyright debian/tmp/usr/share/doc/devuan-keyring/
cd debian/tmp; find -type f \! -regex '.*/DEBIAN/.*' -printf '%P\0' | xargs -r0 md5sum >DEBIAN/md5sums
dpkg-gencontrol -pdevuan-keyring -isp
dpkg-gencontrol: warning: -isp is deprecated; it is without effect
chown -R root.root debian/tmp
chmod -R go=rX debian/tmp
dpkg --build debian/tmp ..
dpkg-deb: building package 'devuan-keyring' in '../devuan-keyring_2022.09.04_all.deb'.
rm -rf debian/devuan-keyring-udeb
install -d -m 755 debian/devuan-keyring-udeb/DEBIAN/
install -d -m 755 debian/devuan-keyring-udeb/usr/share/keyrings/
install -m 644 keyrings/devuan-archive-keyring.gpg debian/devuan-keyring-udeb/usr/share/keyrings/
install -m 755 debian/devuan-keyring-udeb.postinst debian/devuan-keyring-udeb/DEBIAN/postinst
# Don't let dpkg-gencontrol write incorrect guesses to debian/files.
# Instead, register the udeb manually.
dpkg-gencontrol -ndevuan-keyring-udeb_2022.09.04_all.udeb -pdevuan-keyring-udeb -Tdebian/devuan-keyring-udeb.substvars -Pdebian/devuan-keyring-udeb -isp
dpkg-gencontrol: warning: -isp is deprecated; it is without effect
chown -R root.root debian/devuan-keyring-udeb
chmod -R go=rX debian/devuan-keyring-udeb
dpkg --build debian/devuan-keyring-udeb ../devuan-keyring-udeb_2022.09.04_all.udeb
dpkg-deb: building package 'devuan-keyring-udeb' in '../devuan-keyring-udeb_2022.09.04_all.udeb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../devuan-keyring_2022.09.04_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Apr 16 19:47:50 UTC 2023
+ cd ..
+ ls -la
total 136
drwxr-xr-x 3 root root  4096 Apr 16 19:47 .
drwxr-xr-x 3 root root  4096 Apr 16 19:47 ..
-rw-r--r-- 1 root root  4904 Apr 16 19:47 buildlog.txt
drwxr-xr-x 4 root root  4096 Sep  3  2022 devuan-keyring-2022.09.04
-rw-r--r-- 1 root root 10388 Apr 16 19:47 devuan-keyring-udeb_2022.09.04_all.udeb
-rw-r--r-- 1 root root   802 Apr 16 19:47 devuan-keyring_2022.09.04.dsc
-rw-r--r-- 1 root root 53020 Apr 16 19:47 devuan-keyring_2022.09.04.tar.xz
-rw-r--r-- 1 root root 36064 Apr 16 19:47 devuan-keyring_2022.09.04_all.deb
-rw-r--r-- 1 root root  6450 Apr 16 19:47 devuan-keyring_2022.09.04_amd64.buildinfo
-rw-r--r-- 1 root root  2046 Apr 16 19:47 devuan-keyring_2022.09.04_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./devuan-keyring_2022.09.04_amd64.changes ./devuan-keyring_2022.09.04_all.deb ./devuan-keyring_2022.09.04_amd64.buildinfo ./devuan-keyring_2022.09.04.tar.xz ./devuan-keyring-udeb_2022.09.04_all.udeb ./devuan-keyring_2022.09.04.dsc
95d240c2dae6bfbfd570887bc5eb6cc6843d2f6c6c29f6e53737f83dfb2e9a63  ./buildlog.txt
280f69b9c90e93d2deccdbc6eb8e9436f320137e8ee8c421c53cf18784580cb1  ./devuan-keyring_2022.09.04_amd64.changes
ac68e267e20236a5eda173a8628daacae3d4a73dd6ae4f346fc2ca66b78de9c0  ./devuan-keyring_2022.09.04_all.deb
6e6a646919de5c5d7a1dea08ed34e362ee168689594bfe94f3fa53ede0672ed7  ./devuan-keyring_2022.09.04_amd64.buildinfo
82695b21b2e51aba708232b9ed18618df6b75e57679335eb6018131e31b3f8d3  ./devuan-keyring_2022.09.04.tar.xz
bf4021a34a24fd9982c7289a96edd367551bc57ae89df0462bdb3be9956faf7c  ./devuan-keyring-udeb_2022.09.04_all.udeb
98d44dea48fb011a367ed50de37e22d9b5800722629f5f0b2393021fe766ec34  ./devuan-keyring_2022.09.04.dsc
+ mkdir published
+ cd published
+ cd ../
+ ls devuan-keyring_2022.09.04_all.deb devuan-keyring-udeb_2022.09.04_all.udeb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/d/devuan-keyring/devuan-keyring-udeb_2022.09.04_all.udeb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/d/devuan-keyring/devuan-keyring_2022.09.04_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./devuan-keyring_2022.09.04_all.deb ./devuan-keyring-udeb_2022.09.04_all.udeb
96c4a206e8dfdc21138ec619687ef9acf36e1524dd39190c040164f37cc3468d  ./devuan-keyring_2022.09.04_all.deb
bf4021a34a24fd9982c7289a96edd367551bc57ae89df0462bdb3be9956faf7c  ./devuan-keyring-udeb_2022.09.04_all.udeb
+ cd ..
+ sha256sum -c SHA256SUMS
./devuan-keyring_2022.09.04_all.deb: FAILED
./devuan-keyring-udeb_2022.09.04_all.udeb: OK
sha256sum: WARNING: 1 computed checksum did NOT match
