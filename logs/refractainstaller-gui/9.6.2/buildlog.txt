+ date
Mon Apr 17 07:13:33 UTC 2023
+ apt-get source --only-source refractainstaller-gui=9.6.2
Reading package lists...
NOTICE: 'refractainstaller-gui' packaging is maintained in the 'Git' version control system at:
git@git.devuan.org:devuan/refractainstaller-gui.git
Please use:
git clone git@git.devuan.org:devuan/refractainstaller-gui.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 34.0 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main refractainstaller-gui 9.6.2 (dsc) [1251 B]
Get:2 http://deb.devuan.org/merged chimaera/main refractainstaller-gui 9.6.2 (tar) [32.7 kB]
dpkg-source: info: extracting refractainstaller-gui in refractainstaller-gui-9.6.2
dpkg-source: info: unpacking refractainstaller-gui_9.6.2.tar.xz
Fetched 34.0 kB in 0s (121 kB/s)
W: Download is performed unsandboxed as root as file 'refractainstaller-gui_9.6.2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source refractainstaller-gui=9.6.2
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name refractainstaller-gui* -type d
+ cd ./refractainstaller-gui-9.6.2
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package refractainstaller-gui
dpkg-buildpackage: info: source version 9.6.2
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.6.2/refractainstaller-yad"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.6.2/refractainstaller-wrapper.sh"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.6.2/move-dir-mount-gui.sh"
dh clean 
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building refractainstaller-gui in refractainstaller-gui_9.6.2.tar.xz
dpkg-source: info: building refractainstaller-gui in refractainstaller-gui_9.6.2.dsc
 debian/rules build
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.6.2/refractainstaller-yad"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.6.2/refractainstaller-wrapper.sh"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.6.2/move-dir-mount-gui.sh"
dh build 
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.6.2/refractainstaller-yad"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.6.2/refractainstaller-wrapper.sh"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.6.2/move-dir-mount-gui.sh"
dh binary 
   dh_testroot
   dh_prep
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
dpkg-gencontrol: warning: Depends field of package refractainstaller-gui: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'refractainstaller-gui' in '../refractainstaller-gui_9.6.2_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../refractainstaller-gui_9.6.2_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Mon Apr 17 07:13:39 UTC 2023
+ cd ..
+ ls -la
total 100
drwxr-xr-x 3 root root  4096 Apr 17 07:13 .
drwxr-xr-x 3 root root  4096 Apr 17 07:13 ..
-rw-r--r-- 1 root root  3785 Apr 17 07:13 buildlog.txt
drwxr-xr-x 3 root root  4096 Jul  6  2022 refractainstaller-gui-9.6.2
-rw-r--r-- 1 root root   713 Apr 17 07:13 refractainstaller-gui_9.6.2.dsc
-rw-r--r-- 1 root root 32716 Apr 17 07:13 refractainstaller-gui_9.6.2.tar.xz
-rw-r--r-- 1 root root 36156 Apr 17 07:13 refractainstaller-gui_9.6.2_all.deb
-rw-r--r-- 1 root root  5123 Apr 17 07:13 refractainstaller-gui_9.6.2_amd64.buildinfo
-rw-r--r-- 1 root root  1658 Apr 17 07:13 refractainstaller-gui_9.6.2_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./refractainstaller-gui_9.6.2_all.deb ./buildlog.txt ./refractainstaller-gui_9.6.2.tar.xz ./refractainstaller-gui_9.6.2.dsc ./refractainstaller-gui_9.6.2_amd64.changes ./refractainstaller-gui_9.6.2_amd64.buildinfo
b0edd81faa6c9dbf114dff7c1a778f65557e963be51a334205370af88e1d3e92  ./refractainstaller-gui_9.6.2_all.deb
0aeb88717846b16da37729c914365fd739a153f8761f9f6bfb6ec46dd3c4ffe1  ./buildlog.txt
ca51e98699e4f2f6f21da856de3fb9777b523986ed98a681942c4d505a762085  ./refractainstaller-gui_9.6.2.tar.xz
7834c8856053e9234c56b5293e4d309af43ad3b16b09f21ade079f9e86357caa  ./refractainstaller-gui_9.6.2.dsc
923cb1066adc13e9d4500e95a1ec51c90c74457287a9ecd7c55fe90a031bae5e  ./refractainstaller-gui_9.6.2_amd64.changes
a24538a0071dcc21b4e4a049a7c046855944b059e6310db0f67580ebd4f01aad  ./refractainstaller-gui_9.6.2_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls refractainstaller-gui_9.6.2_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/r/refractainstaller-gui/refractainstaller-gui_9.6.2_all.deb
+ + find . -maxdepth 1 -type f
tee ../SHA256SUMS
+ sha256sum ./refractainstaller-gui_9.6.2_all.deb
b0edd81faa6c9dbf114dff7c1a778f65557e963be51a334205370af88e1d3e92  ./refractainstaller-gui_9.6.2_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./refractainstaller-gui_9.6.2_all.deb: OK
+ echo Package refractainstaller-gui version 9.6.2 is reproducible!
Package refractainstaller-gui version 9.6.2 is reproducible!
