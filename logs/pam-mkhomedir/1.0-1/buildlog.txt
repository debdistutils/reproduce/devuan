+ date
Mon Apr 17 06:50:09 UTC 2023
+ apt-get source --only-source pam-mkhomedir=1.0-1
Reading package lists...
NOTICE: 'pam-mkhomedir' packaging is maintained in the 'Git' version control system at:
https://git.devuan.org/devuan/pam-mkhomedir.git
Please use:
git clone https://git.devuan.org/devuan/pam-mkhomedir.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 15.4 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main pam-mkhomedir 1.0-1 (dsc) [1475 B]
Get:2 http://deb.devuan.org/merged chimaera/main pam-mkhomedir 1.0-1 (tar) [7392 B]
Get:3 http://deb.devuan.org/merged chimaera/main pam-mkhomedir 1.0-1 (diff) [6520 B]
dpkg-source: info: extracting pam-mkhomedir in pam-mkhomedir-1.0
dpkg-source: info: unpacking pam-mkhomedir_1.0.orig.tar.gz
dpkg-source: info: unpacking pam-mkhomedir_1.0-1.debian.tar.xz
Fetched 15.4 kB in 0s (126 kB/s)
W: Download is performed unsandboxed as root as file 'pam-mkhomedir_1.0-1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source pam-mkhomedir=1.0-1
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name pam-mkhomedir* -type d
+ cd ./pam-mkhomedir-1.0
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package pam-mkhomedir
dpkg-buildpackage: info: source version 1.0-1
dpkg-buildpackage: info: source distribution UNRELEASED
dpkg-buildpackage: info: source changed by B. Stack <bgstack15@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building pam-mkhomedir using existing ./pam-mkhomedir_1.0.orig.tar.gz
dpkg-source: info: building pam-mkhomedir in pam-mkhomedir_1.0-1.debian.tar.xz
dpkg-source: info: building pam-mkhomedir in pam-mkhomedir_1.0-1.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_dwz
   dh_strip
   dh_makeshlibs
   dh_shlibdeps
   dh_installdeb
   dh_gencontrol
dpkg-gencontrol: warning: Depends field of package pam-mkhomedir: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'pam-mkhomedir' in '../pam-mkhomedir_1.0-1_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../pam-mkhomedir_1.0-1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload (original source is included)
+ date
Mon Apr 17 06:50:15 UTC 2023
+ cd ..
+ ls -la
total 56
drwxr-xr-x 3 root root 4096 Apr 17 06:50 .
drwxr-xr-x 3 root root 4096 Apr 17 06:50 ..
-rw-r--r-- 1 root root 3045 Apr 17 06:50 buildlog.txt
drwxr-xr-x 4 root root 4096 Apr 17 06:50 pam-mkhomedir-1.0
-rw-r--r-- 1 root root 6520 Apr 17 06:50 pam-mkhomedir_1.0-1.debian.tar.xz
-rw-r--r-- 1 root root  937 Apr 17 06:50 pam-mkhomedir_1.0-1.dsc
-rw-r--r-- 1 root root 5046 Apr 17 06:50 pam-mkhomedir_1.0-1_amd64.buildinfo
-rw-r--r-- 1 root root 1763 Apr 17 06:50 pam-mkhomedir_1.0-1_amd64.changes
-rw-r--r-- 1 root root 7248 Apr 17 06:50 pam-mkhomedir_1.0-1_amd64.deb
-rw-r--r-- 1 root root 7392 Jun 10  2021 pam-mkhomedir_1.0.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./pam-mkhomedir_1.0-1_amd64.deb ./buildlog.txt ./pam-mkhomedir_1.0-1_amd64.buildinfo ./pam-mkhomedir_1.0-1.dsc ./pam-mkhomedir_1.0-1.debian.tar.xz ./pam-mkhomedir_1.0.orig.tar.gz ./pam-mkhomedir_1.0-1_amd64.changes
c0d36893dfe12bdd79d0f339c33af17de5724ca974ad655136e7ef9b1619d303  ./pam-mkhomedir_1.0-1_amd64.deb
5be12c3ad0b9cb71aa76a5ee125997793ecf6044908397f4c122fd653bd0552c  ./buildlog.txt
10db123c39f1bf1ed91f9d6e4b4c5e4e61cedca369c7b0c3e4712299615b537f  ./pam-mkhomedir_1.0-1_amd64.buildinfo
98a8d3ff3208bc5a2c3e63bdd5fb9d0a43508fff0c2b7562241656304bd61c8c  ./pam-mkhomedir_1.0-1.dsc
dfb801088672d0f7f02ea88c405e36fe8621c992b8bd758a4bd6444a9377cbd7  ./pam-mkhomedir_1.0-1.debian.tar.xz
7b58e29c93a933c8af421e56eb35be9e6f8a2877b77d1e3cfd197b5d8a90e31c  ./pam-mkhomedir_1.0.orig.tar.gz
fdb08d884d03708c377b97b68621ae74a6553168373bd1892132401cac4e69be  ./pam-mkhomedir_1.0-1_amd64.changes
+ mkdir published
+ cd published
+ cd ../
+ ls pam-mkhomedir_1.0-1_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/p/pam-mkhomedir/pam-mkhomedir_1.0-1_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./pam-mkhomedir_1.0-1_amd64.deb
c0d36893dfe12bdd79d0f339c33af17de5724ca974ad655136e7ef9b1619d303  ./pam-mkhomedir_1.0-1_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./pam-mkhomedir_1.0-1_amd64.deb: OK
+ echo Package pam-mkhomedir version 1.0-1 is reproducible!
Package pam-mkhomedir version 1.0-1 is reproducible!
