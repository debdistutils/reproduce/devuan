+ date
Mon Apr 17 07:34:32 UTC 2023
+ apt-get source --only-source rsyslog=8.2302.0-1~bpo11+1devuan2
Reading package lists...
NOTICE: 'rsyslog' packaging is maintained in the 'Git' version control system at:
https://git.devuan.org/devuan/rsyslog.git
Please use:
git clone https://git.devuan.org/devuan/rsyslog.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 3203 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera-backports/main rsyslog 8.2302.0-1~bpo11+1devuan2 (dsc) [2999 B]
Get:2 http://deb.devuan.org/merged chimaera-backports/main rsyslog 8.2302.0-1~bpo11+1devuan2 (tar) [3169 kB]
Get:3 http://deb.devuan.org/merged chimaera-backports/main rsyslog 8.2302.0-1~bpo11+1devuan2 (diff) [30.9 kB]
dpkg-source: info: extracting rsyslog in rsyslog-8.2302.0
dpkg-source: info: unpacking rsyslog_8.2302.0.orig.tar.gz
dpkg-source: info: unpacking rsyslog_8.2302.0-1~bpo11+1devuan2.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying Dont-fail-tests-process-not-killable.patch
dpkg-source: info: applying Don-t-create-a-database.patch
dpkg-source: info: applying Increase-timeouts-in-imfile-basic-2GB-file-and-imfile-tru.patch
Fetched 3203 kB in 1s (4419 kB/s)
W: Download is performed unsandboxed as root as file 'rsyslog_8.2302.0-1~bpo11+1devuan2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source rsyslog=8.2302.0-1~bpo11+1devuan2
Reading package lists...
Building dependency tree...
Reading state information...
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 libelogind0 : Conflicts: libsystemd0
E: Error, pkgProblemResolver::Resolve generated breaks, this may be caused by held packages.
