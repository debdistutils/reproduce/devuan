+ date
Mon Apr 17 07:44:48 UTC 2023
+ apt-get source --only-source sysvinit=2.96-7+devuan2
Reading package lists...
NOTICE: 'sysvinit' packaging is maintained in the 'Git' version control system at:
https://git.devuan.org/devuan/sysvinit.git
Please use:
git clone https://git.devuan.org/devuan/sysvinit.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 280 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main sysvinit 2.96-7+devuan2 (dsc) [2029 B]
Get:2 http://deb.devuan.org/merged chimaera/main sysvinit 2.96-7+devuan2 (tar) [148 kB]
Get:3 http://deb.devuan.org/merged chimaera/main sysvinit 2.96-7+devuan2 (diff) [130 kB]
dpkg-source: info: extracting sysvinit in sysvinit-2.96
dpkg-source: info: unpacking sysvinit_2.96.orig.tar.gz
dpkg-source: info: unpacking sysvinit_2.96-7+devuan2.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying 0001-Fixed-time-parsing-in-shutdown-when-there-is-a-in-fr.patch
dpkg-source: info: applying 11_run_nologin.patch
dpkg-source: info: applying 0002-Fix-FTBFS-on-kfreebsd-any.patch
dpkg-source: info: applying 0003-Fix-formatting-of-bootlogd-8.patch
dpkg-source: info: applying 0004-fix-manpage-typo.patch
dpkg-source: info: applying 0005-workaround-gcc-lintian.patch
dpkg-source: info: applying fstab-decode.man.patch
dpkg-source: info: applying libcrypt-lib.patch
Fetched 280 kB in 0s (760 kB/s)
W: Download is performed unsandboxed as root as file 'sysvinit_2.96-7+devuan2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source sysvinit=2.96-7+devuan2
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  libselinux1-dev libsepol1-dev
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 507 kB of archives.
After this operation, 2385 kB of additional disk space will be used.
Get:1 http://deb.devuan.org/merged chimaera/main amd64 libsepol1-dev amd64 3.1-1 [338 kB]
Get:2 http://deb.devuan.org/merged chimaera/main amd64 libselinux1-dev amd64 3.1-3 [168 kB]
Fetched 507 kB in 0s (5167 kB/s)
Selecting previously unselected package libsepol1-dev:amd64.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 99471 files and directories currently installed.)
Preparing to unpack .../libsepol1-dev_3.1-1_amd64.deb ...
Unpacking libsepol1-dev:amd64 (3.1-1) ...
Selecting previously unselected package libselinux1-dev:amd64.
Preparing to unpack .../libselinux1-dev_3.1-3_amd64.deb ...
Unpacking libselinux1-dev:amd64 (3.1-3) ...
Setting up libsepol1-dev:amd64 (3.1-1) ...
Setting up libselinux1-dev:amd64 (3.1-3) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name sysvinit* -type d
+ cd ./sysvinit-2.96
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package sysvinit
dpkg-buildpackage: info: source version 2.96-7+devuan2
dpkg-buildpackage: info: source distribution chimaera
dpkg-buildpackage: info: source changed by Mark Hindley <mark@hindley.org.uk>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean 
   dh_auto_clean
	make -j50 distclean
make[1]: Entering directory '/build/sysvinit/sysvinit-2.96'
make VERSION=2.96 -C src distclean
make[2]: Entering directory '/build/sysvinit/sysvinit-2.96/src'
rm -f *.o *.bak
rm -f  init halt shutdown runlevel killall5 fstab-decode logsave sulogin bootlogd last mesg readbootlog utmpdump wall
make[2]: Leaving directory '/build/sysvinit/sysvinit-2.96/src'
make[1]: Leaving directory '/build/sysvinit/sysvinit-2.96'
   debian/rules override_dh_clean
make[1]: Entering directory '/build/sysvinit/sysvinit-2.96'
dh_clean
/usr/bin/make -C src clobber
make[2]: Entering directory '/build/sysvinit/sysvinit-2.96/src'
rm -f *.o *.bak
rm -f  init halt shutdown runlevel killall5 fstab-decode logsave sulogin bootlogd last mesg readbootlog utmpdump wall
make[2]: Leaving directory '/build/sysvinit/sysvinit-2.96/src'
make[1]: Leaving directory '/build/sysvinit/sysvinit-2.96'
 dpkg-source -b .
dpkg-source: warning: upstream signing key but no upstream tarball signature
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building sysvinit using existing ./sysvinit_2.96.orig.tar.gz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: building sysvinit in sysvinit_2.96-7+devuan2.debian.tar.xz
dpkg-source: info: building sysvinit in sysvinit_2.96-7+devuan2.dsc
 debian/rules binary
dh binary 
   debian/rules install-arch
make[1]: Entering directory '/build/sysvinit/sysvinit-2.96'
dh build-arch 
   dh_update_autotools_config -a
   dh_autoreconf -a
   dh_auto_configure -a
   debian/rules override_dh_auto_build
make[2]: Entering directory '/build/sysvinit/sysvinit-2.96'
/usr/bin/make  WITH_SELINUX="yes" DISTRO=Debian LIBDIR=/usr/lib/x86_64-linux-gnu CFLAGS="-g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2" LDFLAGS="-Wl,-z,relro -Wl,-z,now"
make[3]: Entering directory '/build/sysvinit/sysvinit-2.96'
/usr/bin/make VERSION=2.96 -C src all
make[4]: Entering directory '/build/sysvinit/sysvinit-2.96/src'
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF -DWITH_SELINUX  -c -o init.o init.c
In file included from /usr/include/string.h:495,
                 from init.c:53:
In function 'strncpy',
    inlined from 'check_kernel_console' at init.c:1388:6,
    inlined from 'read_inittab' at init.c:1644:3:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:106:10: warning: '__builtin_strncpy' output may be truncated copying 8 bytes from a string of length 31 [-Wstringop-truncation]
  106 |   return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In function 'strncpy',
    inlined from 'check_kernel_console' at init.c:1386:6,
    inlined from 'read_inittab' at init.c:1644:3:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:106:10: warning: '__builtin_strncpy' output may be truncated copying 8 bytes from a string of length 28 [-Wstringop-truncation]
  106 |   return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF -DINIT_MAIN -c -o init_utmp.o utmp.c
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o runlevellog.o runlevellog.c
cc -Wl,-z,relro -Wl,-z,now  init.o init_utmp.o runlevellog.o  -lselinux  -o init
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o halt.o halt.c
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o ifdown.o ifdown.c
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o hddown.o hddown.c
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o utmp.o utmp.c
cc -Wl,-z,relro -Wl,-z,now  halt.o ifdown.o hddown.o utmp.o runlevellog.o   -o halt
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o shutdown.o shutdown.c
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o dowall.o dowall.c
dowall.c: In function 'wall':
dowall.c:204:35: warning: '%s' directive output may be truncated writing up to 64 bytes into a region of size 55 [-Wformat-truncation=]
  204 |    "\r\nBroadcast message from %s@%s %s(%s):\r\n\r\n",
      |                                   ^~
  205 |    user, hostname, tty, date);
      |          ~~~~~~~~                  
In file included from /usr/include/stdio.h:867,
                 from dowall.c:33:
/usr/include/x86_64-linux-gnu/bits/stdio2.h:67:10: note: '__builtin___snprintf_chk' output 35 or more bytes (assuming 99) into a destination of size 81
   67 |   return __builtin___snprintf_chk (__s, __n, __USE_FORTIFY_LEVEL - 1,
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   68 |        __bos (__s), __fmt, __va_arg_pack ());
      |        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cc -Wl,-z,relro -Wl,-z,now  shutdown.o dowall.o utmp.o   -o shutdown
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o runlevel.o runlevel.c
cc -Wl,-z,relro -Wl,-z,now  runlevel.o runlevellog.o   -o runlevel
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF -Wl,-z,relro -Wl,-z,now  killall5.c   -o killall5
In file included from /usr/include/string.h:495,
                 from killall5.c:52:
In function 'strncpy',
    inlined from 'check4nfs' at killall5.c:447:12:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:106:10: warning: '__builtin_strncpy' output may be truncated copying 4096 bytes from a string of length 4096 [-Wstringop-truncation]
  106 |   return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o fstab-decode.o fstab-decode.c
cc -Wl,-z,relro -Wl,-z,now  fstab-decode.o   -o fstab-decode
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o logsave.o logsave.c
cc -Wl,-z,relro -Wl,-z,now  logsave.o   -o logsave
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF -DWITH_SELINUX  -c -o sulogin.o sulogin.c
sulogin.c: In function 'sushell':
sulogin.c:773:3: warning: 'security_context_t' is deprecated [-Wdeprecated-declarations]
  773 |   security_context_t scon=NULL;
      |   ^~~~~~~~~~~~~~~~~~
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o consoles.o consoles.c
cc -Wl,-z,relro -Wl,-z,now  sulogin.o consoles.o  -lselinux	 -lcrypt  -o sulogin
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o bootlogd.o bootlogd.c
cc -Wl,-z,relro -Wl,-z,now  bootlogd.o  -lutil  -o bootlogd
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o last.o last.c
cc -Wl,-z,relro -Wl,-z,now  last.o   -o last
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o mesg.o mesg.c
cc -Wl,-z,relro -Wl,-z,now  mesg.o   -o mesg
cc -g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -ansi -fomit-frame-pointer -fstack-protector-strong -W -Wall -Wunreachable-code -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -D_XOPEN_SOURCE -D_GNU_SOURCE -DVERSION=\"2.96\"  -DACCTON_OFF  -c -o readbootlog.o readbootlog.c
cc -Wl,-z,relro -Wl,-z,now  readbootlog.o   -o readbootlog
make[4]: Leaving directory '/build/sysvinit/sysvinit-2.96/src'
make[3]: Leaving directory '/build/sysvinit/sysvinit-2.96'
make[2]: Leaving directory '/build/sysvinit/sysvinit-2.96'
   dh_auto_test -a
   create-stamp debian/debhelper-build-stamp
dh install-arch 
   dh_prep -a
   dh_installdirs -a
   debian/rules override_dh_auto_install-arch
make[2]: Entering directory '/build/sysvinit/sysvinit-2.96'
/usr/bin/make -C src  ROOT=/build/sysvinit/sysvinit-2.96/debian/tmp DISTRO=Debian install
make[3]: Entering directory '/build/sysvinit/sysvinit-2.96/src'
install -m 755 -d /build/sysvinit/sysvinit-2.96/debian/tmp/bin/ /build/sysvinit/sysvinit-2.96/debian/tmp/sbin/
install -m 755 -d /build/sysvinit/sysvinit-2.96/debian/tmp/usr/bin/
for i in ; do \
		install -o root -g root -m 755 $i /build/sysvinit/sysvinit-2.96/debian/tmp/bin/ ; \
	done
for i in init halt shutdown runlevel killall5 fstab-decode logsave sulogin bootlogd; do \
		install -o root -g root -m 755 $i /build/sysvinit/sysvinit-2.96/debian/tmp/sbin/ ; \
	done
for i in last mesg readbootlog; do \
		install -o root -g root -m 755 $i /build/sysvinit/sysvinit-2.96/debian/tmp/usr/bin/ ; \
	done
# install -m 755 -d /build/sysvinit/sysvinit-2.96/debian/tmp/etc/
# install -o root -g root -m 755 ../doc/initscript.sample /build/sysvinit/sysvinit-2.96/debian/tmp/etc/
ln -sf halt /build/sysvinit/sysvinit-2.96/debian/tmp/sbin/reboot
ln -sf halt /build/sysvinit/sysvinit-2.96/debian/tmp/sbin/poweroff
ln -sf init /build/sysvinit/sysvinit-2.96/debian/tmp/sbin/telinit
ln -sf /sbin/killall5 /build/sysvinit/sysvinit-2.96/debian/tmp/bin/pidof
if [ ! -f /build/sysvinit/sysvinit-2.96/debian/tmp/usr/bin/lastb ]; then \
		ln -sf last /build/sysvinit/sysvinit-2.96/debian/tmp/usr/bin/lastb; \
	fi
install -m 755 -d /build/sysvinit/sysvinit-2.96/debian/tmp/usr/include/
install -o root -g root -m 644 initreq.h /build/sysvinit/sysvinit-2.96/debian/tmp/usr/include/
install -m 755 -d /build/sysvinit/sysvinit-2.96/debian/tmp/usr/share/man/man1/
install -m 755 -d /build/sysvinit/sysvinit-2.96/debian/tmp/usr/share/man/man5/
install -m 755 -d /build/sysvinit/sysvinit-2.96/debian/tmp/usr/share/man/man8/
for man in last.1 lastb.1 mesg.1 readbootlog.1; do \
		install -o root -g root -m 644 ../man/$man /build/sysvinit/sysvinit-2.96/debian/tmp/usr/share/man/man1/; \
		sed -i "1{ ; }" /build/sysvinit/sysvinit-2.96/debian/tmp/usr/share/man/man1/$man ; \
	done
for man in initscript.5 inittab.5 initctl.5; do \
		install -o root -g root -m 644 ../man/$man /build/sysvinit/sysvinit-2.96/debian/tmp/usr/share/man/man5/; \
		sed -i "1{ ; }" /build/sysvinit/sysvinit-2.96/debian/tmp/usr/share/man/man5/$man ; \
	done
for man in halt.8 init.8 killall5.8 pidof.8 poweroff.8 reboot.8 runlevel.8 shutdown.8 telinit.8 fstab-decode.8 logsave.8 sulogin.8 bootlogd.8; do \
		install -o root -g root -m 644 ../man/$man /build/sysvinit/sysvinit-2.96/debian/tmp/usr/share/man/man8/; \
		sed -i "1{ ; }" /build/sysvinit/sysvinit-2.96/debian/tmp/usr/share/man/man8/$man ; \
	done
make[3]: Leaving directory '/build/sysvinit/sysvinit-2.96/src'
make[2]: Leaving directory '/build/sysvinit/sysvinit-2.96'
   debian/rules override_dh_install-arch
make[2]: Entering directory '/build/sysvinit/sysvinit-2.96'
dh_install
# sysvinit package
if test -e debian/share/inittab.x86_64-linux-gnu ; \
then \
	install -m 644 \
		debian/share/inittab.x86_64-linux-gnu \
		/build/sysvinit/sysvinit-2.96/debian/sysvinit-core/usr/share/sysvinit/inittab ; \
elif test -e debian/share/inittab.linux-gnu ; \
then \
	install -m 644 \
		debian/share/inittab.linux-gnu \
		/build/sysvinit/sysvinit-2.96/debian/sysvinit-core/usr/share/sysvinit/inittab ; \
else \
	install -m 644 debian/share/inittab \
		/build/sysvinit/sysvinit-2.96/debian/sysvinit-core/usr/share/sysvinit/inittab ; \
fi
# initscripts package may include /sys
install -m 755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/sys
make[2]: Leaving directory '/build/sysvinit/sysvinit-2.96'
   dh_installdocs -a
   debian/rules override_dh_installchangelogs-arch
make[2]: Entering directory '/build/sysvinit/sysvinit-2.96'
dh_installchangelogs -psysvinit-core doc/Changelog
dh_installchangelogs -psysvinit-utils
rm -f /build/sysvinit/sysvinit-2.96/debian/sysvinit-utils/usr/share/doc/sysvinit-utils/changelog
sed -i -ne '/sysvinit (2.93-8)/q' -e p \
	/build/sysvinit/sysvinit-2.96/debian/sysvinit-utils/usr/share/doc/sysvinit-utils/changelog.Debian
dh_installchangelogs -pbootlogd
sed -i -ne '/sysvinit (2.93-8)/q' -e p \
	/build/sysvinit/sysvinit-2.96/debian/bootlogd/usr/share/doc/bootlogd/changelog.Debian
make[2]: Leaving directory '/build/sysvinit/sysvinit-2.96'
   dh_installman -a
   dh_installdebconf -a
   dh_lintian -a
   dh_perl -a
   dh_link -a
   dh_strip_nondeterminism -a
   dh_compress -a
   dh_fixperms -a
   dh_missing -a
dh_missing: warning: bin/pidof exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: sbin/logsave exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: sbin/poweroff exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: sbin/reboot exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: sbin/sulogin exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: sbin/telinit exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/bin/last exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/bin/lastb exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/bin/mesg exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/man1/last.1 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/man1/lastb.1 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/man1/mesg.1 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/man5/initctl.5 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/man8/logsave.8 exists in debian/tmp but is not installed to anywhere 
dh_missing: warning: usr/share/man/man8/sulogin.8 exists in debian/tmp but is not installed to anywhere 
	The following debhelper tools have reported what they installed (with files per package)
	 * dh_install: bootlogd (5), initscripts (0), sysv-rc (2), sysvinit (0), sysvinit-core (14), sysvinit-utils (7)
	 * dh_installdocs: bootlogd (0), initscripts (0), sysv-rc (8), sysvinit (0), sysvinit-core (0), sysvinit-utils (0)
	 * dh_installman: bootlogd (2), initscripts (0), sysv-rc (0), sysvinit (0), sysvinit-core (0), sysvinit-utils (1)
	If the missing files are installed by another tool, please file a bug against it.
	When filing the report, if the tool is not part of debhelper itself, please reference the
	"Logging helpers and dh_missing" section from the "PROGRAMMING" guide for debhelper (10.6.3+).
	  (in the debhelper package: /usr/share/doc/debhelper/PROGRAMMING.gz)
	Be sure to test with dpkg-buildpackage -A/-B as the results may vary when only a subset is built
	If the omission is intentional or no other helper can take care of this consider adding the
	paths to debian/not-installed.
   dh_dwz -a
   dh_strip -a
   dh_makeshlibs -a
   dh_shlibdeps -a
make[1]: Leaving directory '/build/sysvinit/sysvinit-2.96'
   dh_update_autotools_config -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_autoreconf -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_auto_configure -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   debian/rules override_dh_auto_build
make[1]: Entering directory '/build/sysvinit/sysvinit-2.96'
/usr/bin/make  WITH_SELINUX="yes" DISTRO=Debian LIBDIR=/usr/lib/x86_64-linux-gnu CFLAGS="-g -O2 -ffile-prefix-map=/build/sysvinit/sysvinit-2.96=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2" LDFLAGS="-Wl,-z,relro -Wl,-z,now"
make[2]: Entering directory '/build/sysvinit/sysvinit-2.96'
/usr/bin/make VERSION=2.96 -C src all
make[3]: Entering directory '/build/sysvinit/sysvinit-2.96/src'
make[3]: Nothing to be done for 'all'.
make[3]: Leaving directory '/build/sysvinit/sysvinit-2.96/src'
make[2]: Leaving directory '/build/sysvinit/sysvinit-2.96'
make[1]: Leaving directory '/build/sysvinit/sysvinit-2.96'
   dh_auto_test -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   create-stamp debian/debhelper-build-stamp
   dh_prep -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   debian/rules override_dh_auto_install-indep
make[1]: Entering directory '/build/sysvinit/sysvinit-2.96'
/usr/bin/make -C debian/src/initscripts install DESTDIR=/build/sysvinit/sysvinit-2.96/debian/initscripts
make[2]: Entering directory '/build/sysvinit/sysvinit-2.96/debian/src/initscripts'
install -m755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/lib/init/.
install -m755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/var/lib/initscripts/.
install -m755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/var/lib/urandom/.
install -m755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/var/log/fsck/.
install -m755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/usr/share/doc/initscripts/.
install -m644 doc/* /build/sysvinit/sysvinit-2.96/debian/initscripts/usr/share/doc/initscripts
install -m755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/etc/.
install -m755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/etc/default
cp -afv etc/* /build/sysvinit/sysvinit-2.96/debian/initscripts/etc
'etc/default/devpts' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/default/devpts'
'etc/default/halt' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/default/halt'
'etc/default/rcS' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/default/rcS'
'etc/default/tmpfs' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/default/tmpfs'
'etc/init.d' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d'
'etc/init.d/bootlogs' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/bootlogs'
'etc/init.d/bootmisc.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/bootmisc.sh'
'etc/init.d/brightness' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/brightness'
'etc/init.d/checkfs.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/checkfs.sh'
'etc/init.d/checkroot-bootclean.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/checkroot-bootclean.sh'
'etc/init.d/checkroot.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/checkroot.sh'
'etc/init.d/halt' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/halt'
'etc/init.d/hostname.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/hostname.sh'
'etc/init.d/killprocs' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/killprocs'
'etc/init.d/mount-configfs' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/mount-configfs'
'etc/init.d/mountall-bootclean.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/mountall-bootclean.sh'
'etc/init.d/mountall.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/mountall.sh'
'etc/init.d/mountdevsubfs.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/mountdevsubfs.sh'
'etc/init.d/mountkernfs.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/mountkernfs.sh'
'etc/init.d/mountnfs-bootclean.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/mountnfs-bootclean.sh'
'etc/init.d/mountnfs.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/mountnfs.sh'
'etc/init.d/rc.local' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/rc.local'
'etc/init.d/reboot' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/reboot'
'etc/init.d/rmnologin' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/rmnologin'
'etc/init.d/sendsigs' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/sendsigs'
'etc/init.d/single' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/single'
'etc/init.d/umountfs' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/umountfs'
'etc/init.d/umountnfs.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/umountnfs.sh'
'etc/init.d/umountroot' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/umountroot'
'etc/init.d/urandom' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/urandom'
'etc/network' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/network'
'etc/network/if-up.d' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/network/if-up.d'
'etc/network/if-up.d/mountnfs' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/network/if-up.d/mountnfs'
'etc/rc.local' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/etc/rc.local'
find /build/sysvinit/sysvinit-2.96/debian/initscripts/etc -type d -name .svn -print0 |xargs -r0 rm -r
cp -afv lib/init/* /build/sysvinit/sysvinit-2.96/debian/initscripts/lib/init
'lib/init/bootclean.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/lib/init/bootclean.sh'
'lib/init/mount-functions.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/lib/init/mount-functions.sh'
'lib/init/tmpfs.sh' -> '/build/sysvinit/sysvinit-2.96/debian/initscripts/lib/init/tmpfs.sh'
find /build/sysvinit/sysvinit-2.96/debian/initscripts/lib -type d -name .svn  -print0 |xargs -r0 rm -r
chmod 755 /build/sysvinit/sysvinit-2.96/debian/initscripts/etc/init.d/[a-z]*
chmod 755 /build/sysvinit/sysvinit-2.96/debian/initscripts/etc/network/if-up.d/[a-z]*
chmod 755 /build/sysvinit/sysvinit-2.96/debian/initscripts/etc/rc.local
chmod 644 /build/sysvinit/sysvinit-2.96/debian/initscripts/lib/init/*.sh
chmod -R g-w /build/sysvinit/sysvinit-2.96/debian/initscripts
install -m755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/sbin/.
install -m755 sbin/fsck.nfs /build/sysvinit/sysvinit-2.96/debian/initscripts/sbin/fsck.nfs
install -m755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/usr/share/man/man8
install -m644 man/fsck.nfs.8 \
	/build/sysvinit/sysvinit-2.96/debian/initscripts/usr/share/man/man8/fsck.nfs.8
install -m755 -d /build/sysvinit/sysvinit-2.96/debian/initscripts/usr/share/man/man5
install -m644 man/*.5 /build/sysvinit/sysvinit-2.96/debian/initscripts/usr/share/man/man5/.
make[2]: Leaving directory '/build/sysvinit/sysvinit-2.96/debian/src/initscripts'
make[1]: Leaving directory '/build/sysvinit/sysvinit-2.96'
   dh_install -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_installdocs -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   debian/rules override_dh_installchangelogs-indep
make[1]: Entering directory '/build/sysvinit/sysvinit-2.96'
dh_installchangelogs
sed -i -ne '/sysvinit (2.93-8)/q' -e p \
	/build/sysvinit/sysvinit-2.96/debian/sysv-rc/usr/share/doc/sysv-rc/changelog.Debian
dh_installchangelogs -pinitscripts
sed -i -ne '/sysvinit (2.93-8)/q' -e p \
	/build/sysvinit/sysvinit-2.96/debian/initscripts/usr/share/doc/initscripts/changelog.Debian
make[1]: Leaving directory '/build/sysvinit/sysvinit-2.96'
   dh_installman -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_lintian -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_perl -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_link -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_strip_nondeterminism -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_compress -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_fixperms -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_missing -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_dwz -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_strip -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_makeshlibs -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   dh_shlibdeps -Nsysvinit -Nsysvinit-core -Nsysvinit-utils -Nbootlogd
   debian/rules override_dh_installdeb
make[1]: Entering directory '/build/sysvinit/sysvinit-2.96'
dh_installdeb
make[1]: Leaving directory '/build/sysvinit/sysvinit-2.96'
   dh_gencontrol
dpkg-gencontrol: warning: Depends field of package sysvinit: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'sysvinit' in '../sysvinit_2.96-7+devuan2_amd64.deb'.
dpkg-deb: building package 'sysvinit-core' in '../sysvinit-core_2.96-7+devuan2_amd64.deb'.
dpkg-deb: building package 'sysvinit-utils' in '../sysvinit-utils_2.96-7+devuan2_amd64.deb'.
dpkg-deb: building package 'sysv-rc' in '../sysv-rc_2.96-7+devuan2_all.deb'.
dpkg-deb: building package 'initscripts' in '../initscripts_2.96-7+devuan2_all.deb'.
dpkg-deb: building package 'bootlogd' in '../bootlogd_2.96-7+devuan2_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../sysvinit_2.96-7+devuan2_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Mon Apr 17 07:45:20 UTC 2023
+ cd ..
+ ls -la
total 664
drwxr-xr-x 3 root root   4096 Apr 17 07:45 .
drwxr-xr-x 3 root root   4096 Apr 17 07:44 ..
-rw-r--r-- 1 root root  40340 Apr 17 07:45 bootlogd_2.96-7+devuan2_amd64.deb
-rw-r--r-- 1 root root  33525 Apr 17 07:45 buildlog.txt
-rw-r--r-- 1 root root  59104 Apr 17 07:45 initscripts_2.96-7+devuan2_all.deb
-rw-r--r-- 1 root root  34636 Apr 17 07:45 sysv-rc_2.96-7+devuan2_all.deb
drwxr-xr-x 8 root root   4096 Apr 17 07:44 sysvinit-2.96
-rw-r--r-- 1 root root 155872 Apr 17 07:45 sysvinit-core_2.96-7+devuan2_amd64.deb
-rw-r--r-- 1 root root  25772 Apr 17 07:45 sysvinit-utils_2.96-7+devuan2_amd64.deb
-rw-r--r-- 1 root root 130308 Apr 17 07:44 sysvinit_2.96-7+devuan2.debian.tar.xz
-rw-r--r-- 1 root root   1491 Apr 17 07:44 sysvinit_2.96-7+devuan2.dsc
-rw-r--r-- 1 root root   6686 Apr 17 07:45 sysvinit_2.96-7+devuan2_amd64.buildinfo
-rw-r--r-- 1 root root   3384 Apr 17 07:45 sysvinit_2.96-7+devuan2_amd64.changes
-rw-r--r-- 1 root root   2168 Apr 17 07:45 sysvinit_2.96-7+devuan2_amd64.deb
-rw-r--r-- 1 root root 147834 Apr  5  2020 sysvinit_2.96.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./sysvinit-core_2.96-7+devuan2_amd64.deb ./sysvinit_2.96.orig.tar.gz ./sysvinit_2.96-7+devuan2_amd64.deb ./buildlog.txt ./initscripts_2.96-7+devuan2_all.deb ./sysvinit-utils_2.96-7+devuan2_amd64.deb ./sysvinit_2.96-7+devuan2_amd64.changes ./sysvinit_2.96-7+devuan2_amd64.buildinfo ./sysvinit_2.96-7+devuan2.debian.tar.xz ./bootlogd_2.96-7+devuan2_amd64.deb ./sysv-rc_2.96-7+devuan2_all.deb ./sysvinit_2.96-7+devuan2.dsc
a674e8b396eaf6e5083d5e292dd9255b8c9558b9a06596d0b120829b874c3a03  ./sysvinit-core_2.96-7+devuan2_amd64.deb
1275620f767c85bb2d7e5b9542579ae097f3eb542065ff30a70efc95b6e84c64  ./sysvinit_2.96.orig.tar.gz
819ed772d345c4fa7c9f9b53dead98da5916451efa27ea32e356eb51a74c1399  ./sysvinit_2.96-7+devuan2_amd64.deb
a060020aed70121f42ea4403e62202f157d096d36423a080af26d749f1dc1354  ./buildlog.txt
0cf53fea661873254dc0c0bba1539925ab4812eb14d8f9a9e5625497fe57c508  ./initscripts_2.96-7+devuan2_all.deb
63819de97fdc923330a0d6649b725c83526189dbd5ad6dc40195f0111d83d24a  ./sysvinit-utils_2.96-7+devuan2_amd64.deb
c577a79c42a3b6f494839a0ec76b2a3708d207f85432360e4017803bbdc4df92  ./sysvinit_2.96-7+devuan2_amd64.changes
dafdcc8c5d641a42eba809a746af0bfbc8538413efce8d8d148b3e1fd08b51f6  ./sysvinit_2.96-7+devuan2_amd64.buildinfo
c20e7b4777cfc6eefc1b1cdc66f63b0680b22ddb80316608470ea491f9b8ad9b  ./sysvinit_2.96-7+devuan2.debian.tar.xz
0e90ee4da48bee8960e0f797b3a2375cf5828eaa26fcb6ccb15672540032760a  ./bootlogd_2.96-7+devuan2_amd64.deb
73cc6a52d5baecc764b571ca284717bc172663a82eb3a5756a074921e8363be3  ./sysv-rc_2.96-7+devuan2_all.deb
6aa9804f6824ca80b48651e09e02dc7484c03214f66f62fe07f032323e9cc70b  ./sysvinit_2.96-7+devuan2.dsc
+ mkdir published
+ cd published
+ cd ../
+ ls bootlogd_2.96-7+devuan2_amd64.deb initscripts_2.96-7+devuan2_all.deb sysv-rc_2.96-7+devuan2_all.deb sysvinit-core_2.96-7+devuan2_amd64.deb sysvinit-utils_2.96-7+devuan2_amd64.deb sysvinit_2.96-7+devuan2_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/s/sysvinit/bootlogd_2.96-7+devuan2_amd64.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/s/sysvinit/initscripts_2.96-7+devuan2_all.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/s/sysvinit/sysv-rc_2.96-7+devuan2_all.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/s/sysvinit/sysvinit-core_2.96-7+devuan2_amd64.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/s/sysvinit/sysvinit-utils_2.96-7+devuan2_amd64.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/s/sysvinit/sysvinit_2.96-7+devuan2_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./sysvinit-core_2.96-7+devuan2_amd64.deb ./sysvinit_2.96-7+devuan2_amd64.deb ./initscripts_2.96-7+devuan2_all.deb ./sysvinit-utils_2.96-7+devuan2_amd64.deb ./bootlogd_2.96-7+devuan2_amd64.deb ./sysv-rc_2.96-7+devuan2_all.deb
a674e8b396eaf6e5083d5e292dd9255b8c9558b9a06596d0b120829b874c3a03  ./sysvinit-core_2.96-7+devuan2_amd64.deb
819ed772d345c4fa7c9f9b53dead98da5916451efa27ea32e356eb51a74c1399  ./sysvinit_2.96-7+devuan2_amd64.deb
0cf53fea661873254dc0c0bba1539925ab4812eb14d8f9a9e5625497fe57c508  ./initscripts_2.96-7+devuan2_all.deb
63819de97fdc923330a0d6649b725c83526189dbd5ad6dc40195f0111d83d24a  ./sysvinit-utils_2.96-7+devuan2_amd64.deb
0e90ee4da48bee8960e0f797b3a2375cf5828eaa26fcb6ccb15672540032760a  ./bootlogd_2.96-7+devuan2_amd64.deb
73cc6a52d5baecc764b571ca284717bc172663a82eb3a5756a074921e8363be3  ./sysv-rc_2.96-7+devuan2_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./sysvinit-core_2.96-7+devuan2_amd64.deb: OK
./sysvinit_2.96-7+devuan2_amd64.deb: OK
./initscripts_2.96-7+devuan2_all.deb: OK
./sysvinit-utils_2.96-7+devuan2_amd64.deb: OK
./bootlogd_2.96-7+devuan2_amd64.deb: OK
./sysv-rc_2.96-7+devuan2_all.deb: OK
+ echo Package sysvinit version 2.96-7+devuan2 is reproducible!
Package sysvinit version 2.96-7+devuan2 is reproducible!
