+ date
Sun Apr 16 19:37:05 UTC 2023
+ apt-get source --only-source debootstrap=1.0.123+devuan3
Reading package lists...
NOTICE: 'debootstrap' packaging is maintained in the 'Git' version control system at:
https://git.devuan.org/devuan/debootstrap.git
Please use:
git clone https://git.devuan.org/devuan/debootstrap.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 78.7 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main debootstrap 1.0.123+devuan3 (dsc) [1645 B]
Get:2 http://deb.devuan.org/merged chimaera/main debootstrap 1.0.123+devuan3 (tar) [77.1 kB]
dpkg-source: info: extracting debootstrap in debootstrap-1.0.123+devuan3
dpkg-source: info: unpacking debootstrap_1.0.123+devuan3.tar.gz
Fetched 78.7 kB in 0s (287 kB/s)
W: Download is performed unsandboxed as root as file 'debootstrap_1.0.123+devuan3.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source debootstrap=1.0.123+devuan3
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name debootstrap* -type d
+ cd ./debootstrap-1.0.123+devuan3
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package debootstrap
dpkg-buildpackage: info: source version 1.0.123+devuan3
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Mark Hindley <mark@hindley.org.uk>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
dpkg-source: info: using options from debootstrap-1.0.123+devuan3/debian/source/options: --compression=gzip
 debian/rules clean
dh clean
   dh_auto_clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using options from debootstrap-1.0.123+devuan3/debian/source/options: --compression=gzip
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building debootstrap in debootstrap_1.0.123+devuan3.tar.gz
dpkg-source: info: building debootstrap in debootstrap_1.0.123+devuan3.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
   dh_auto_test
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
dh_auto_build
	make -j50 "INSTALL=install --strip-program=true"
make[2]: Entering directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
make[2]: Nothing to be done for 'all'.
make[2]: Leaving directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
/usr/bin/make install DESTDIR=/build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap
make[2]: Entering directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
mkdir -p /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap/usr/share/debootstrap/scripts
mkdir -p /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap/usr/sbin
cp -a scripts/* /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap/usr/share/debootstrap/scripts/
install -o root -g root -m 0644 functions /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap/usr/share/debootstrap/
sed 's/@VERSION@/1.0.123+devuan3/g' debootstrap >/build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap/usr/sbin/debootstrap
chown root:root /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap/usr/sbin/debootstrap
chmod 0755 /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap/usr/sbin/debootstrap
make[2]: Leaving directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
/usr/bin/make install DESTDIR=/build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap-udeb
make[2]: Entering directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
mkdir -p /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap-udeb/usr/share/debootstrap/scripts
mkdir -p /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap-udeb/usr/sbin
cp -a scripts/* /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap-udeb/usr/share/debootstrap/scripts/
install -o root -g root -m 0644 functions /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap-udeb/usr/share/debootstrap/
sed 's/@VERSION@/1.0.123+devuan3/g' debootstrap >/build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap-udeb/usr/sbin/debootstrap
chown root:root /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap-udeb/usr/sbin/debootstrap
chmod 0755 /build/debootstrap/debootstrap-1.0.123+devuan3/debian/debootstrap-udeb/usr/sbin/debootstrap
make[2]: Leaving directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
make[1]: Leaving directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
   dh_installdocs
   dh_installchangelogs
   dh_installman
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   debian/rules override_dh_gencontrol
make[1]: Entering directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
dh_gencontrol -- -Vkeyring=devuan-keyring
make[1]: Leaving directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
   dh_md5sums
   debian/rules override_dh_builddeb
make[1]: Entering directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
dh_builddeb -pdebootstrap      -- -Zgzip
dpkg-deb: building package 'debootstrap' in '../debootstrap_1.0.123+devuan3_all.deb'.
dh_builddeb -pdebootstrap-udeb -- -Zxz
dpkg-deb: building package 'debootstrap-udeb' in 'debian/.debhelper/scratch-space/build-debootstrap-udeb/debootstrap-udeb_1.0.123+devuan3_all.deb'.
	Renaming debootstrap-udeb_1.0.123+devuan3_all.deb to debootstrap-udeb_1.0.123+devuan3_all.udeb
make[1]: Leaving directory '/build/debootstrap/debootstrap-1.0.123+devuan3'
 dpkg-genbuildinfo
 dpkg-genchanges  >../debootstrap_1.0.123+devuan3_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-source: info: using options from debootstrap-1.0.123+devuan3/debian/source/options: --compression=gzip
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Apr 16 19:37:15 UTC 2023
+ cd ..
+ ls -la
total 216
drwxr-xr-x 3 root root  4096 Apr 16 19:37 .
drwxr-xr-x 3 root root  4096 Apr 16 19:37 ..
-rw-r--r-- 1 root root  6350 Apr 16 19:37 buildlog.txt
drwxr-xr-x 4 root root  4096 Dec 11  2020 debootstrap-1.0.123+devuan3
-rw-r--r-- 1 root root 27140 Apr 16 19:37 debootstrap-udeb_1.0.123+devuan3_all.udeb
-rw-r--r-- 1 root root  1107 Apr 16 19:37 debootstrap_1.0.123+devuan3.dsc
-rw-r--r-- 1 root root 77107 Apr 16 19:37 debootstrap_1.0.123+devuan3.tar.gz
-rw-r--r-- 1 root root 76130 Apr 16 19:37 debootstrap_1.0.123+devuan3_all.deb
-rw-r--r-- 1 root root  5409 Apr 16 19:37 debootstrap_1.0.123+devuan3_amd64.buildinfo
-rw-r--r-- 1 root root  2055 Apr 16 19:37 debootstrap_1.0.123+devuan3_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./debootstrap_1.0.123+devuan3_amd64.changes ./debootstrap-udeb_1.0.123+devuan3_all.udeb ./buildlog.txt ./debootstrap_1.0.123+devuan3.dsc ./debootstrap_1.0.123+devuan3_all.deb ./debootstrap_1.0.123+devuan3.tar.gz ./debootstrap_1.0.123+devuan3_amd64.buildinfo
c527779cc297fb06d8ad3219a61be52684d247260ff97ac1e2479ab380ce289a  ./debootstrap_1.0.123+devuan3_amd64.changes
9dbd160dc611d9a33bfc9416cfa18b76182d1e9e030afa8b64bb0f78a28bc5d1  ./debootstrap-udeb_1.0.123+devuan3_all.udeb
ebb76304083ac7f89b707b42792aec81a51cae43d5919b835747d1725b47d4ce  ./buildlog.txt
53f2342df3490bb248e64bdbdb14c394b8d86c4760d5abb92be1b600a9f94e6d  ./debootstrap_1.0.123+devuan3.dsc
9db4512fc719af0d9259b2ca04db0c42929f5bee78b7120c916de8de3a7cf186  ./debootstrap_1.0.123+devuan3_all.deb
aeb4a0ace0cb252b87490c35cfd0b11b16f74615b9139765e239c303083bd79d  ./debootstrap_1.0.123+devuan3.tar.gz
dcdd0a884c9db1adad5ab2ab63f4a4a0815941d0db7bf8e5d3fcfacb4ef7da77  ./debootstrap_1.0.123+devuan3_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls debootstrap_1.0.123+devuan3_all.deb debootstrap-udeb_1.0.123+devuan3_all.udeb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/d/debootstrap/debootstrap-udeb_1.0.123+devuan3_all.udeb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/d/debootstrap/debootstrap_1.0.123+devuan3_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./debootstrap-udeb_1.0.123+devuan3_all.udeb ./debootstrap_1.0.123+devuan3_all.deb
9dbd160dc611d9a33bfc9416cfa18b76182d1e9e030afa8b64bb0f78a28bc5d1  ./debootstrap-udeb_1.0.123+devuan3_all.udeb
9db4512fc719af0d9259b2ca04db0c42929f5bee78b7120c916de8de3a7cf186  ./debootstrap_1.0.123+devuan3_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./debootstrap-udeb_1.0.123+devuan3_all.udeb: OK
./debootstrap_1.0.123+devuan3_all.deb: OK
+ echo Package debootstrap version 1.0.123+devuan3 is reproducible!
Package debootstrap version 1.0.123+devuan3 is reproducible!
