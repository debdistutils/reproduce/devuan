+ date
Sun Apr 16 20:43:15 UTC 2023
+ apt-get source --only-source ltspfs=1.5-2+devuan1
Reading package lists...
NOTICE: 'ltspfs' packaging is maintained in the 'Git' version control system at:
https://git.devuan.org/devuan/ltspfs.git
Please use:
git clone https://git.devuan.org/devuan/ltspfs.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 48.6 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main ltspfs 1.5-2+devuan1 (dsc) [1584 B]
Get:2 http://deb.devuan.org/merged chimaera/main ltspfs 1.5-2+devuan1 (tar) [38.1 kB]
Get:3 http://deb.devuan.org/merged chimaera/main ltspfs 1.5-2+devuan1 (diff) [8924 B]
dpkg-source: info: extracting ltspfs in ltspfs-1.5
dpkg-source: info: unpacking ltspfs_1.5.orig.tar.gz
dpkg-source: info: unpacking ltspfs_1.5-2+devuan1.debian.tar.xz
Fetched 48.6 kB in 0s (356 kB/s)
W: Download is performed unsandboxed as root as file 'ltspfs_1.5-2+devuan1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source ltspfs=1.5-2+devuan1
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  libblkid-dev libfuse-dev libglib2.0-bin libglib2.0-dev libglib2.0-dev-bin
  libmount-dev libpthread-stubs0-dev libselinux1-dev libsepol1-dev libx11-dev
  libxau-dev libxcb1-dev libxdmcp-dev uuid-dev x11proto-dev xorg-sgml-doctools
  xtrans-dev
0 upgraded, 17 newly installed, 0 to remove and 0 not upgraded.
Need to get 5673 kB of archives.
After this operation, 27.4 MB of additional disk space will be used.
Get:1 http://deb.devuan.org/merged chimaera/main amd64 uuid-dev amd64 2.36.1-8+devuan2 [111 kB]
Get:2 http://deb.devuan.org/merged chimaera/main amd64 libblkid-dev amd64 2.36.1-8+devuan2 [236 kB]
Get:8 http://deb.devuan.org/merged chimaera/main amd64 libmount-dev amd64 2.36.1-8+devuan2 [89.2 kB]
Get:3 http://deb.devuan.org/merged chimaera/main amd64 libsepol1-dev amd64 3.1-1 [338 kB]
Get:4 http://deb.devuan.org/merged chimaera/main amd64 libselinux1-dev amd64 3.1-3 [168 kB]
Get:5 http://deb.devuan.org/merged chimaera/main amd64 libfuse-dev amd64 2.9.9-5 [1031 kB]
Get:6 http://deb.devuan.org/merged chimaera/main amd64 libglib2.0-bin amd64 2.66.8-1 [141 kB]
Get:7 http://deb.devuan.org/merged chimaera/main amd64 libglib2.0-dev-bin amd64 2.66.8-1 [179 kB]
Get:9 http://deb.devuan.org/merged chimaera/main amd64 libglib2.0-dev amd64 2.66.8-1 [1577 kB]
Get:10 http://deb.devuan.org/merged chimaera/main amd64 libpthread-stubs0-dev amd64 0.4-1 [5344 B]
Get:11 http://deb.devuan.org/merged chimaera/main amd64 xorg-sgml-doctools all 1:1.11-1.1 [22.1 kB]
Get:12 http://deb.devuan.org/merged chimaera/main amd64 x11proto-dev all 2020.1-1 [594 kB]
Get:13 http://deb.devuan.org/merged chimaera/main amd64 libxau-dev amd64 1:1.0.9-1 [22.9 kB]
Get:14 http://deb.devuan.org/merged chimaera/main amd64 libxdmcp-dev amd64 1:1.1.2-3 [42.2 kB]
Get:15 http://deb.devuan.org/merged chimaera/main amd64 xtrans-dev all 1.4.0-1 [98.7 kB]
Get:16 http://deb.devuan.org/merged chimaera/main amd64 libxcb1-dev amd64 1.14-3 [176 kB]
Get:17 http://deb.devuan.org/merged chimaera/main amd64 libx11-dev amd64 2:1.7.2-1 [841 kB]
Fetched 5673 kB in 2s (2643 kB/s)
Selecting previously unselected package uuid-dev:amd64.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 99471 files and directories currently installed.)
Preparing to unpack .../00-uuid-dev_2.36.1-8+devuan2_amd64.deb ...
Unpacking uuid-dev:amd64 (2.36.1-8+devuan2) ...
Selecting previously unselected package libblkid-dev:amd64.
Preparing to unpack .../01-libblkid-dev_2.36.1-8+devuan2_amd64.deb ...
Unpacking libblkid-dev:amd64 (2.36.1-8+devuan2) ...
Selecting previously unselected package libsepol1-dev:amd64.
Preparing to unpack .../02-libsepol1-dev_3.1-1_amd64.deb ...
Unpacking libsepol1-dev:amd64 (3.1-1) ...
Selecting previously unselected package libselinux1-dev:amd64.
Preparing to unpack .../03-libselinux1-dev_3.1-3_amd64.deb ...
Unpacking libselinux1-dev:amd64 (3.1-3) ...
Selecting previously unselected package libfuse-dev:amd64.
Preparing to unpack .../04-libfuse-dev_2.9.9-5_amd64.deb ...
Unpacking libfuse-dev:amd64 (2.9.9-5) ...
Selecting previously unselected package libglib2.0-bin.
Preparing to unpack .../05-libglib2.0-bin_2.66.8-1_amd64.deb ...
Unpacking libglib2.0-bin (2.66.8-1) ...
Selecting previously unselected package libglib2.0-dev-bin.
Preparing to unpack .../06-libglib2.0-dev-bin_2.66.8-1_amd64.deb ...
Unpacking libglib2.0-dev-bin (2.66.8-1) ...
Selecting previously unselected package libmount-dev:amd64.
Preparing to unpack .../07-libmount-dev_2.36.1-8+devuan2_amd64.deb ...
Unpacking libmount-dev:amd64 (2.36.1-8+devuan2) ...
Selecting previously unselected package libglib2.0-dev:amd64.
Preparing to unpack .../08-libglib2.0-dev_2.66.8-1_amd64.deb ...
Unpacking libglib2.0-dev:amd64 (2.66.8-1) ...
Selecting previously unselected package libpthread-stubs0-dev:amd64.
Preparing to unpack .../09-libpthread-stubs0-dev_0.4-1_amd64.deb ...
Unpacking libpthread-stubs0-dev:amd64 (0.4-1) ...
Selecting previously unselected package xorg-sgml-doctools.
Preparing to unpack .../10-xorg-sgml-doctools_1%3a1.11-1.1_all.deb ...
Unpacking xorg-sgml-doctools (1:1.11-1.1) ...
Selecting previously unselected package x11proto-dev.
Preparing to unpack .../11-x11proto-dev_2020.1-1_all.deb ...
Unpacking x11proto-dev (2020.1-1) ...
Selecting previously unselected package libxau-dev:amd64.
Preparing to unpack .../12-libxau-dev_1%3a1.0.9-1_amd64.deb ...
Unpacking libxau-dev:amd64 (1:1.0.9-1) ...
Selecting previously unselected package libxdmcp-dev:amd64.
Preparing to unpack .../13-libxdmcp-dev_1%3a1.1.2-3_amd64.deb ...
Unpacking libxdmcp-dev:amd64 (1:1.1.2-3) ...
Selecting previously unselected package xtrans-dev.
Preparing to unpack .../14-xtrans-dev_1.4.0-1_all.deb ...
Unpacking xtrans-dev (1.4.0-1) ...
Selecting previously unselected package libxcb1-dev:amd64.
Preparing to unpack .../15-libxcb1-dev_1.14-3_amd64.deb ...
Unpacking libxcb1-dev:amd64 (1.14-3) ...
Selecting previously unselected package libx11-dev:amd64.
Preparing to unpack .../16-libx11-dev_2%3a1.7.2-1_amd64.deb ...
Unpacking libx11-dev:amd64 (2:1.7.2-1) ...
Setting up libglib2.0-dev-bin (2.66.8-1) ...
Setting up libsepol1-dev:amd64 (3.1-1) ...
Setting up libglib2.0-bin (2.66.8-1) ...
Setting up libpthread-stubs0-dev:amd64 (0.4-1) ...
Setting up xtrans-dev (1.4.0-1) ...
Setting up uuid-dev:amd64 (2.36.1-8+devuan2) ...
Setting up xorg-sgml-doctools (1:1.11-1.1) ...
Setting up libblkid-dev:amd64 (2.36.1-8+devuan2) ...
Setting up libselinux1-dev:amd64 (3.1-3) ...
Setting up libfuse-dev:amd64 (2.9.9-5) ...
Setting up libmount-dev:amd64 (2.36.1-8+devuan2) ...
Setting up libglib2.0-dev:amd64 (2.66.8-1) ...
Processing triggers for libglib2.0-0:amd64 (2.66.8-1) ...
Processing triggers for man-db (2.9.4-2) ...
Processing triggers for sgml-base (1.30) ...
Setting up x11proto-dev (2020.1-1) ...
Setting up libxau-dev:amd64 (1:1.0.9-1) ...
Setting up libxdmcp-dev:amd64 (1:1.1.2-3) ...
Setting up libxcb1-dev:amd64 (1.14-3) ...
Setting up libx11-dev:amd64 (2:1.7.2-1) ...
+ find . -maxdepth 1 -name ltspfs* -type d
+ cd ./ltspfs-1.5
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package ltspfs
dpkg-buildpackage: info: source version 1.5-2+devuan1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Mark Hindley <mark@hindley.org.uk>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building ltspfs using existing ./ltspfs_1.5.orig.tar.gz
dpkg-source: info: building ltspfs in ltspfs_1.5-2+devuan1.debian.tar.xz
dpkg-source: info: building ltspfs in ltspfs_1.5-2+devuan1.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   debian/rules override_dh_autoreconf
make[1]: Entering directory '/build/ltspfs/ltspfs-1.5'
touch ChangeLog
dh_autoreconf
configure.ac:9: installing './compile'
configure.ac:4: installing './install-sh'
configure.ac:4: installing './missing'
src/Makefile.am: installing './depcomp'
make[1]: Leaving directory '/build/ltspfs/ltspfs-1.5'
   dh_auto_configure
	./configure --build=x86_64-linux-gnu --prefix=/usr --includedir=\${prefix}/include --mandir=\${prefix}/share/man --infodir=\${prefix}/share/info --sysconfdir=/etc --localstatedir=/var --disable-option-checking --disable-silent-rules --libdir=\${prefix}/lib/x86_64-linux-gnu --libexecdir=\${prefix}/lib/x86_64-linux-gnu --runstatedir=/run --disable-maintainer-mode --disable-dependency-tracking
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a thread-safe mkdir -p... /bin/mkdir -p
checking for gawk... gawk
checking whether make sets $(MAKE)... yes
checking whether make supports nested variables... yes
checking whether to enable maintainer-specific portions of Makefiles... no
checking for gcc... gcc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables... 
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... yes
checking for gcc option to accept ISO C89... none needed
checking whether gcc understands -c and -o together... yes
checking whether make supports the include directive... yes (GNU style)
checking dependency style of gcc... none
checking for pkg-config... /usr/bin/pkg-config
checking pkg-config is at least version 0.9.0... yes
checking for fuse, x11... yes
checking how to run the C preprocessor... gcc -E
checking for grep that handles long lines and -e... /bin/grep
checking for egrep... /bin/grep -E
checking for ANSI C header files... yes
checking for dirent.h that defines DIR... yes
checking for library containing opendir... none required
checking for sys/wait.h that is POSIX.1 compatible... yes
checking for sys/types.h... yes
checking for sys/stat.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for memory.h... yes
checking for strings.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for unistd.h... yes
checking fcntl.h usability... yes
checking fcntl.h presence... yes
checking for fcntl.h... yes
checking for glib-2.0 >= 2.6.0... yes
checking for gobject-2.0 >= 2.6.0... yes
checking that generated files are newer than configure... done
configure: creating ./config.status
config.status: creating Makefile
config.status: creating src/Makefile
config.status: creating man/Makefile
config.status: creating scripts/Makefile
config.status: creating udev/Makefile
config.status: creating config.h
config.status: executing depfiles commands
   dh_auto_build
	make -j50
make[1]: Entering directory '/build/ltspfs/ltspfs-1.5'
make  all-recursive
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5'
Making all in src
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5/src'
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -W -D_FILE_OFFSET_BITS=64 -DFUSE_USE_VERSION=22 -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o ltspfs-ltspfs.o `test -f 'ltspfs.c' || echo './'`ltspfs.c
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -W -D_FILE_OFFSET_BITS=64 -DFUSE_USE_VERSION=22 -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o ltspfs-common.o `test -f 'common.c' || echo './'`common.c
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -W -D_FILE_OFFSET_BITS=64 -DAUTOMOUNT -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o ltspfsd-ltspfsd.o `test -f 'ltspfsd.c' || echo './'`ltspfsd.c
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -W -D_FILE_OFFSET_BITS=64 -DAUTOMOUNT -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o ltspfsd-ltspfsd_functions.o `test -f 'ltspfsd_functions.c' || echo './'`ltspfsd_functions.c
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -W -D_FILE_OFFSET_BITS=64 -DAUTOMOUNT -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o ltspfsd-common.o `test -f 'common.c' || echo './'`common.c
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2  -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o lbmount.o lbmount.c
In file included from /usr/include/string.h:495,
                 from ltspfsd_functions.c:24:
In function 'strncpy',
    inlined from 'get_fn' at ltspfsd_functions.c:98:5:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:106:10: warning: '__builtin_strncpy' specified bound depends on the length of the source argument [-Wstringop-overflow=]
  106 |   return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ltspfsd_functions.c: In function 'get_fn':
ltspfsd_functions.c:96:11: note: length computed here
   96 |     mpl = strlen(mountpoint);
      |           ^~~~~~~~~~~~~~~~~~
gcc  -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security  -Wl,-z,relro -Wl,-z,now -o lbmount lbmount.o  
gcc -Wall -W -D_FILE_OFFSET_BITS=64 -DAUTOMOUNT -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security  -Wl,-z,relro -Wl,-z,now -o ltspfsd ltspfsd-ltspfsd.o ltspfsd-ltspfsd_functions.o ltspfsd-common.o  
gcc -Wall -W -D_FILE_OFFSET_BITS=64 -DFUSE_USE_VERSION=22 -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -pthread -Wl,-z,relro -Wl,-z,now -o ltspfs ltspfs-ltspfs.o ltspfs-common.o -lfuse -pthread -lX11 
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5/src'
Making all in man
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5/man'
make[3]: Nothing to be done for 'all'.
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5/man'
Making all in scripts
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5/scripts'
make[3]: Nothing to be done for 'all'.
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5/scripts'
Making all in udev
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5/udev'
make[3]: Nothing to be done for 'all'.
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5/udev'
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5'
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5'
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5'
make[1]: Leaving directory '/build/ltspfs/ltspfs-1.5'
   dh_auto_test
	make -j50 check VERBOSE=1
make[1]: Entering directory '/build/ltspfs/ltspfs-1.5'
Making check in src
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5/src'
make[2]: Nothing to be done for 'check'.
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5/src'
Making check in man
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5/man'
make[2]: Nothing to be done for 'check'.
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5/man'
Making check in scripts
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5/scripts'
make[2]: Nothing to be done for 'check'.
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5/scripts'
Making check in udev
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5/udev'
make[2]: Nothing to be done for 'check'.
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5/udev'
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5'
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5'
make[1]: Leaving directory '/build/ltspfs/ltspfs-1.5'
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   dh_auto_install
	make -j50 install DESTDIR=/build/ltspfs/ltspfs-1.5/debian/tmp AM_UPDATE_INFO_DIR=no
make[1]: Entering directory '/build/ltspfs/ltspfs-1.5'
Making install in src
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5/src'
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5/src'
make[3]: Nothing to be done for 'install-data-am'.
 /bin/mkdir -p '/build/ltspfs/ltspfs-1.5/debian/tmp/usr/bin'
  /usr/bin/install -c ltspfs ltspfsd lbmount '/build/ltspfs/ltspfs-1.5/debian/tmp/usr/bin'
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5/src'
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5/src'
Making install in man
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5/man'
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5/man'
make[3]: Nothing to be done for 'install-exec-am'.
 /bin/mkdir -p '/build/ltspfs/ltspfs-1.5/debian/tmp/usr/share/man/man1'
 /usr/bin/install -c -m 644 lbmount.1 ltspfs.1 ltspfsd.1 ltspfsmounter.1 ltspfs_mount.1 ltspfs_umount.1 '/build/ltspfs/ltspfs-1.5/debian/tmp/usr/share/man/man1'
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5/man'
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5/man'
Making install in scripts
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5/scripts'
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5/scripts'
/bin/mkdir -p /build/ltspfs/ltspfs-1.5/debian/tmp/lib/udev
 /bin/mkdir -p '/build/ltspfs/ltspfs-1.5/debian/tmp/usr/sbin'
 /bin/mkdir -p '/build/ltspfs/ltspfs-1.5/debian/tmp/usr/share/ldm/rc.d'
 /bin/mkdir -p '/build/ltspfs/ltspfs-1.5/debian/tmp/usr/share/ltsp/xinitrc.d'
/usr/bin/install -c -m 644 -m 0755 -D ./ltspfs_entry /build/ltspfs/ltspfs-1.5/debian/tmp/lib/udev
 /usr/bin/install -c -m 644 ldm/X10-delayed-mounter ldm/X98-delayed-mounter '/build/ltspfs/ltspfs-1.5/debian/tmp/usr/share/ldm/rc.d'
 /usr/bin/install -c -m 644 xinitrc.d/I05-set-ltspfs_token '/build/ltspfs/ltspfs-1.5/debian/tmp/usr/share/ltsp/xinitrc.d'
 /usr/bin/install -c ltspfs_mount ltspfsmounter ltspfs_umount '/build/ltspfs/ltspfs-1.5/debian/tmp/usr/sbin'
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5/scripts'
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5/scripts'
Making install in udev
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5/udev'
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5/udev'
make[3]: Nothing to be done for 'install-exec-am'.
/bin/mkdir -p /build/ltspfs/ltspfs-1.5/debian/tmp/usr/share/ltspfs/udev/
/usr/bin/install -c -m 644 ./ltspfsd.rules /build/ltspfs/ltspfs-1.5/debian/tmp/usr/share/ltspfs/udev/
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5/udev'
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5/udev'
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5'
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5'
make[3]: Nothing to be done for 'install-exec-am'.
make[3]: Nothing to be done for 'install-data-am'.
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5'
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5'
make[1]: Leaving directory '/build/ltspfs/ltspfs-1.5'
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_installexamples
   dh_installman
   debian/rules override_dh_installinit
make[1]: Entering directory '/build/ltspfs/ltspfs-1.5'
dh_installinit --noscripts
make[1]: Leaving directory '/build/ltspfs/ltspfs-1.5'
   dh_installsystemd
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_strip
   dh_makeshlibs
   dh_shlibdeps
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'ltspfsd' in '../ltspfsd_1.5-2+devuan1_all.deb'.
dpkg-deb: building package 'ltspfs' in '../ltspfs_1.5-2+devuan1_amd64.deb'.
dpkg-deb: building package 'ltspfsd-core' in '../ltspfsd-core_1.5-2+devuan1_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../ltspfs_1.5-2+devuan1_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Sun Apr 16 20:43:42 UTC 2023
+ cd ..
+ ls -la
total 164
drwxr-xr-x  3 root root  4096 Apr 16 20:43 .
drwxr-xr-x  3 root root  4096 Apr 16 20:43 ..
-rw-r--r--  1 root root 20894 Apr 16 20:43 buildlog.txt
drwxr-xr-x 11 root root  4096 Apr 16 20:43 ltspfs-1.5
-rw-r--r--  1 root root  8924 Apr 16 20:43 ltspfs_1.5-2+devuan1.debian.tar.xz
-rw-r--r--  1 root root  1046 Apr 16 20:43 ltspfs_1.5-2+devuan1.dsc
-rw-r--r--  1 root root  7221 Apr 16 20:43 ltspfs_1.5-2+devuan1_amd64.buildinfo
-rw-r--r--  1 root root  2249 Apr 16 20:43 ltspfs_1.5-2+devuan1_amd64.changes
-rw-r--r--  1 root root 23704 Apr 16 20:43 ltspfs_1.5-2+devuan1_amd64.deb
-rw-r--r--  1 root root 38121 Jun 29  2020 ltspfs_1.5.orig.tar.gz
-rw-r--r--  1 root root 23060 Apr 16 20:43 ltspfsd-core_1.5-2+devuan1_amd64.deb
-rw-r--r--  1 root root 11316 Apr 16 20:43 ltspfsd_1.5-2+devuan1_all.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./ltspfs_1.5-2+devuan1_amd64.changes ./ltspfs_1.5-2+devuan1_amd64.buildinfo ./buildlog.txt ./ltspfsd-core_1.5-2+devuan1_amd64.deb ./ltspfs_1.5.orig.tar.gz ./ltspfsd_1.5-2+devuan1_all.deb ./ltspfs_1.5-2+devuan1_amd64.deb ./ltspfs_1.5-2+devuan1.dsc ./ltspfs_1.5-2+devuan1.debian.tar.xz
cdec206f728d04a0a2bb369116524c0821b4a3b75fd57fe3aefe644ba1bc5d00  ./ltspfs_1.5-2+devuan1_amd64.changes
9c3e0476097cdbfe54393235184f518ba63307ae5d3d0c46e9e392253e6adebb  ./ltspfs_1.5-2+devuan1_amd64.buildinfo
d769f84494a2910e9730e7de8f630d9da9a171d47774d033cff486831136392e  ./buildlog.txt
3ab78f9f9a74f788becd4a0220ae6dd37e7c0fdd3fd89d2e39c721409dfa887b  ./ltspfsd-core_1.5-2+devuan1_amd64.deb
64e1cce9ba63f6c352f7b0f3129daff0f85604939475900b827965897c108bac  ./ltspfs_1.5.orig.tar.gz
ca72281beb63cfd497b19565278fdecc1027baf06540e3624d0a6f9e45c3dafe  ./ltspfsd_1.5-2+devuan1_all.deb
572057fc6ff3f9575cf66eda75f41f31a4b1d196fefa5391f670a3f1f613422e  ./ltspfs_1.5-2+devuan1_amd64.deb
36ef6f2d43fb10dac3e32c93ec6ebaf2ff261c5a8b6d77c86beadc72da5ccdcc  ./ltspfs_1.5-2+devuan1.dsc
25e3b9694da28b854200fc636f685444d95e79f612fe50cc47004234afda604d  ./ltspfs_1.5-2+devuan1.debian.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls ltspfs_1.5-2+devuan1_amd64.deb ltspfsd-core_1.5-2+devuan1_amd64.deb ltspfsd_1.5-2+devuan1_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/l/ltspfs/ltspfs_1.5-2+devuan1_amd64.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/l/ltspfs/ltspfsd-core_1.5-2+devuan1_amd64.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/l/ltspfs/ltspfsd_1.5-2+devuan1_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./ltspfsd-core_1.5-2+devuan1_amd64.deb ./ltspfsd_1.5-2+devuan1_all.deb ./ltspfs_1.5-2+devuan1_amd64.deb
13b2975397820f5d0c1cf5dfef7e63e73decf58c9d28a51633e531d7f7cb6c55  ./ltspfsd-core_1.5-2+devuan1_amd64.deb
38210447ecde25a06814ed133cea50f9e8ae24a7e3a30dd15802d3d0618e00b1  ./ltspfsd_1.5-2+devuan1_all.deb
f38d5c3f33417e6b87e13148a4ba5886f5316590abb47423d7f4370551adc427  ./ltspfs_1.5-2+devuan1_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./ltspfsd-core_1.5-2+devuan1_amd64.deb: FAILED
./ltspfsd_1.5-2+devuan1_all.deb: FAILED
./ltspfs_1.5-2+devuan1_amd64.deb: FAILED
sha256sum: WARNING: 3 computed checksums did NOT match
