+ date
Sun Apr 16 20:12:02 UTC 2023
+ apt-get source --only-source fontsnaps=1.4
Reading package lists...
NOTICE: 'fontsnaps' packaging is maintained in the 'Git' version control system at:
git@git.devuan.org:devuan-packages/fontsnaps.git
Please use:
git clone git@git.devuan.org:devuan-packages/fontsnaps.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 10.1 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main fontsnaps 1.4 (dsc) [1209 B]
Get:2 http://deb.devuan.org/merged chimaera/main fontsnaps 1.4 (tar) [8856 B]
dpkg-source: info: extracting fontsnaps in fontsnaps-1.4
dpkg-source: info: unpacking fontsnaps_1.4.tar.xz
Fetched 10.1 kB in 0s (38.1 kB/s)
W: Download is performed unsandboxed as root as file 'fontsnaps_1.4.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source fontsnaps=1.4
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name fontsnaps* -type d
+ cd ./fontsnaps-1.4
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package fontsnaps
dpkg-buildpackage: info: source version 1.4
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
chmod 0655 "/build/fontsnaps/fontsnaps-1.4/adjfontsize"
dh clean 
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building fontsnaps in fontsnaps_1.4.tar.xz
dpkg-source: info: building fontsnaps in fontsnaps_1.4.dsc
 debian/rules build
chmod 0655 "/build/fontsnaps/fontsnaps-1.4/adjfontsize"
dh build 
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
chmod 0655 "/build/fontsnaps/fontsnaps-1.4/adjfontsize"
dh binary 
   dh_testroot
   dh_prep
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
dpkg-gencontrol: warning: Depends field of package fontsnaps: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'fontsnaps' in '../fontsnaps_1.4_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../fontsnaps_1.4_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Apr 16 20:12:11 UTC 2023
+ cd ..
+ ls -la
total 56
drwxr-xr-x 3 root root 4096 Apr 16 20:12 .
drwxr-xr-x 3 root root 4096 Apr 16 20:12 ..
-rw-r--r-- 1 root root 2832 Apr 16 20:12 buildlog.txt
drwxr-xr-x 3 root root 4096 Jul 18  2019 fontsnaps-1.4
-rw-r--r-- 1 root root  671 Apr 16 20:12 fontsnaps_1.4.dsc
-rw-r--r-- 1 root root 8856 Apr 16 20:12 fontsnaps_1.4.tar.xz
-rw-r--r-- 1 root root 9876 Apr 16 20:12 fontsnaps_1.4_all.deb
-rw-r--r-- 1 root root 4984 Apr 16 20:12 fontsnaps_1.4_amd64.buildinfo
-rw-r--r-- 1 root root 1393 Apr 16 20:12 fontsnaps_1.4_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./fontsnaps_1.4.dsc ./fontsnaps_1.4_all.deb ./buildlog.txt ./fontsnaps_1.4_amd64.changes ./fontsnaps_1.4_amd64.buildinfo ./fontsnaps_1.4.tar.xz
0f013f943d65f5cad726927d04da53214ddd07a546c1a013583705a848fd6448  ./fontsnaps_1.4.dsc
fe5f24f97216e0c8292dc11fe2d86a12b965144aba5042004ebb7d207d1a7bab  ./fontsnaps_1.4_all.deb
55d822095744145b4f7d89581bfa561302db0f5d20d0f99b569233b604e900f3  ./buildlog.txt
ca8ed1b06be63060017c7a21a1468d23727d15245e3a104520c5aa7f9b57204a  ./fontsnaps_1.4_amd64.changes
c9d1790dc3a86fd980a5f92ebd5ee74c20ce9a6bac01a40fef879776ee379f20  ./fontsnaps_1.4_amd64.buildinfo
e0a7262126b3accef96ff89f79d179e139e13697e924d50872816138f5aa24a6  ./fontsnaps_1.4.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls fontsnaps_1.4_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/f/fontsnaps/fontsnaps_1.4_all.deb
+ tee ../SHA256SUMS+ 
find . -maxdepth 1 -type f
+ sha256sum ./fontsnaps_1.4_all.deb
fe5f24f97216e0c8292dc11fe2d86a12b965144aba5042004ebb7d207d1a7bab  ./fontsnaps_1.4_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./fontsnaps_1.4_all.deb: OK
+ echo Package fontsnaps version 1.4 is reproducible!
Package fontsnaps version 1.4 is reproducible!
