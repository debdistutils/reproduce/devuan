+ date
Sun Apr 16 18:09:25 UTC 2023
+ apt-get source --only-source clearlooks-phenix-darkpurpy-theme=7.0.2-1+devuan3.0
Reading package lists...
NOTICE: 'clearlooks-phenix-darkpurpy-theme' packaging is maintained in the 'Git' version control system at:
https://git.devuan.org/devuan-packages/clearlooks-phenix-darkpurpy-theme.git
Please use:
git clone https://git.devuan.org/devuan-packages/clearlooks-phenix-darkpurpy-theme.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 352 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main clearlooks-phenix-darkpurpy-theme 7.0.2-1+devuan3.0 (dsc) [1835 B]
Get:2 http://deb.devuan.org/merged chimaera/main clearlooks-phenix-darkpurpy-theme 7.0.2-1+devuan3.0 (tar) [345 kB]
Get:3 http://deb.devuan.org/merged chimaera/main clearlooks-phenix-darkpurpy-theme 7.0.2-1+devuan3.0 (diff) [5000 B]
dpkg-source: info: extracting clearlooks-phenix-darkpurpy-theme in clearlooks-phenix-darkpurpy-theme-7.0.2
dpkg-source: info: unpacking clearlooks-phenix-darkpurpy-theme_7.0.2.orig.tar.gz
dpkg-source: info: unpacking clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0.debian.tar.xz
Fetched 352 kB in 2s (193 kB/s)
W: Download is performed unsandboxed as root as file 'clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source clearlooks-phenix-darkpurpy-theme=7.0.2-1+devuan3.0
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name clearlooks-phenix-darkpurpy-theme* -type d
+ cd ./clearlooks-phenix-darkpurpy-theme-7.0.2
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package clearlooks-phenix-darkpurpy-theme
dpkg-buildpackage: info: source version 7.0.2-1+devuan3.0
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_clean
dh_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building clearlooks-phenix-darkpurpy-theme using existing ./clearlooks-phenix-darkpurpy-theme_7.0.2.orig.tar.gz
dpkg-source: info: building clearlooks-phenix-darkpurpy-theme in clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0.debian.tar.xz
dpkg-source: info: building clearlooks-phenix-darkpurpy-theme in clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0.dsc
 debian/rules build
dh build
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_update_autotools_config
 debian/rules binary
dh binary
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_testroot
   dh_prep
   dh_install
dh_install: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installchangelogs
dh_installchangelogs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
dh_compress: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   debian/rules override_dh_fixperms
make[1]: Entering directory '/build/clearlooks-phenix-darkpurpy-theme/clearlooks-phenix-darkpurpy-theme-7.0.2'
dh_fixperms
find debian/*/usr/share/themes -type f -print0 2>/dev/null | xargs -0r chmod 644
make[1]: Leaving directory '/build/clearlooks-phenix-darkpurpy-theme/clearlooks-phenix-darkpurpy-theme-7.0.2'
   dh_missing
dh_missing: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdeb
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'clearlooks-phenix-darkpurpy-theme' in '../clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload (original source is included)
+ date
Sun Apr 16 18:09:35 UTC 2023
+ cd ..
+ ls -la
total 656
drwxr-xr-x  3 root root   4096 Apr 16 18:09 .
drwxr-xr-x  3 root root   4096 Apr 16 18:09 ..
-rw-r--r--  1 root root   4630 Apr 16 18:09 buildlog.txt
drwxr-xr-x 11 root root   4096 Apr 16 18:09 clearlooks-phenix-darkpurpy-theme-7.0.2
-rw-r--r--  1 root root   5000 Apr 16 18:09 clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0.debian.tar.xz
-rw-r--r--  1 root root   1297 Apr 16 18:09 clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0.dsc
-rw-r--r--  1 root root 277748 Apr 16 18:09 clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_all.deb
-rw-r--r--  1 root root   5333 Apr 16 18:09 clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_amd64.buildinfo
-rw-r--r--  1 root root   2474 Apr 16 18:09 clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_amd64.changes
-rw-r--r--  1 root root 345447 Nov 17  2019 clearlooks-phenix-darkpurpy-theme_7.0.2.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_amd64.changes ./clearlooks-phenix-darkpurpy-theme_7.0.2.orig.tar.gz ./buildlog.txt ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_amd64.buildinfo ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_all.deb ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0.dsc ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0.debian.tar.xz
53bd73143f0ed73306a497036ba81e54773992cc4682669292578c60c737a8d8  ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_amd64.changes
a04eeac2187fb9adbd3d79ceac4efec4733a6b4be8687bdbc4bb166362958470  ./clearlooks-phenix-darkpurpy-theme_7.0.2.orig.tar.gz
5bd392ada9198c82b711bcb05db3055ad49880e6f3c0e4af40ea0ca73f3decc4  ./buildlog.txt
e2bb7b5a8589e071a7e2c8f80456d3a76da8b6d37dc06553cc1a836db714c50f  ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_amd64.buildinfo
517550f00d6f52ad4525d5b6ede54587891825801bd6da5dd040253223e0d213  ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_all.deb
bd2454b66f32961eb687586de4794ba21f7dcafc64485562759aaab05585fc55  ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0.dsc
9cb612df3a287b95d102a61d99b1f38a55f0e4c622e92623d5e6b46444b3030e  ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0.debian.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/c/clearlooks-phenix-darkpurpy-theme/clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_all.deb
+ + tee ../SHA256SUMS
find . -maxdepth 1 -type f
+ sha256sum ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_all.deb
517550f00d6f52ad4525d5b6ede54587891825801bd6da5dd040253223e0d213  ./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./clearlooks-phenix-darkpurpy-theme_7.0.2-1+devuan3.0_all.deb: OK
+ echo Package clearlooks-phenix-darkpurpy-theme version 7.0.2-1+devuan3.0 is reproducible!
Package clearlooks-phenix-darkpurpy-theme version 7.0.2-1+devuan3.0 is reproducible!
