+ date
Sun Apr 16 18:09:23 UTC 2023
+ apt-get source --only-source clearlooks-phenix-cinnabar-theme=7.0.1-5
Reading package lists...
NOTICE: 'clearlooks-phenix-cinnabar-theme' packaging is maintained in the 'Git' version control system at:
https://git.devuan.org/devuan-packages/clearlooks-phenix-cinnabar-theme.git
Please use:
git clone https://git.devuan.org/devuan-packages/clearlooks-phenix-cinnabar-theme.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 147 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main clearlooks-phenix-cinnabar-theme 7.0.1-5 (dsc) [1862 B]
Get:2 http://deb.devuan.org/merged chimaera/main clearlooks-phenix-cinnabar-theme 7.0.1-5 (tar) [140 kB]
Get:3 http://deb.devuan.org/merged chimaera/main clearlooks-phenix-cinnabar-theme 7.0.1-5 (diff) [5064 B]
dpkg-source: info: extracting clearlooks-phenix-cinnabar-theme in clearlooks-phenix-cinnabar-theme-7.0.1
dpkg-source: info: unpacking clearlooks-phenix-cinnabar-theme_7.0.1.orig.tar.gz
dpkg-source: info: unpacking clearlooks-phenix-cinnabar-theme_7.0.1-5.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying color-change
dpkg-source: info: applying top-left-active-fix
Fetched 147 kB in 0s (609 kB/s)
W: Download is performed unsandboxed as root as file 'clearlooks-phenix-cinnabar-theme_7.0.1-5.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source clearlooks-phenix-cinnabar-theme=7.0.1-5
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name clearlooks-phenix-cinnabar-theme* -type d
+ cd ./clearlooks-phenix-cinnabar-theme-7.0.1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package clearlooks-phenix-cinnabar-theme
dpkg-buildpackage: info: source version 7.0.1-5
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building clearlooks-phenix-cinnabar-theme using existing ./clearlooks-phenix-cinnabar-theme_7.0.1.orig.tar.gz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: building clearlooks-phenix-cinnabar-theme in clearlooks-phenix-cinnabar-theme_7.0.1-5.debian.tar.xz
dpkg-source: info: building clearlooks-phenix-cinnabar-theme in clearlooks-phenix-cinnabar-theme_7.0.1-5.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   debian/rules override_dh_fixperms
make[1]: Entering directory '/build/clearlooks-phenix-cinnabar-theme/clearlooks-phenix-cinnabar-theme-7.0.1'
dh_fixperms
find debian/*/usr/share/themes -type f -print0 2>/dev/null | xargs -0r chmod 644
make[1]: Leaving directory '/build/clearlooks-phenix-cinnabar-theme/clearlooks-phenix-cinnabar-theme-7.0.1'
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'clearlooks-phenix-cinnabar-theme' in '../clearlooks-phenix-cinnabar-theme_7.0.1-5_all.deb'.
dpkg-deb: building package 'gtk3-nooverlayscrollbar' in '../gtk3-nooverlayscrollbar_7.0.1-5_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../clearlooks-phenix-cinnabar-theme_7.0.1-5_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Sun Apr 16 18:09:31 UTC 2023
+ cd ..
+ ls -la
total 280
drwxr-xr-x  3 root root   4096 Apr 16 18:09 .
drwxr-xr-x  3 root root   4096 Apr 16 18:09 ..
-rw-r--r--  1 root root   4048 Apr 16 18:09 buildlog.txt
drwxr-xr-x 11 root root   4096 Apr 16 18:09 clearlooks-phenix-cinnabar-theme-7.0.1
-rw-r--r--  1 root root   5064 Apr 16 18:09 clearlooks-phenix-cinnabar-theme_7.0.1-5.debian.tar.xz
-rw-r--r--  1 root root   1324 Apr 16 18:09 clearlooks-phenix-cinnabar-theme_7.0.1-5.dsc
-rw-r--r--  1 root root  91972 Apr 16 18:09 clearlooks-phenix-cinnabar-theme_7.0.1-5_all.deb
-rw-r--r--  1 root root   5551 Apr 16 18:09 clearlooks-phenix-cinnabar-theme_7.0.1-5_amd64.buildinfo
-rw-r--r--  1 root root   2226 Apr 16 18:09 clearlooks-phenix-cinnabar-theme_7.0.1-5_amd64.changes
-rw-r--r--  1 root root 140307 Nov 17  2019 clearlooks-phenix-cinnabar-theme_7.0.1.orig.tar.gz
-rw-r--r--  1 root root   4184 Apr 16 18:09 gtk3-nooverlayscrollbar_7.0.1-5_all.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./clearlooks-phenix-cinnabar-theme_7.0.1-5.debian.tar.xz ./buildlog.txt ./clearlooks-phenix-cinnabar-theme_7.0.1-5.dsc ./clearlooks-phenix-cinnabar-theme_7.0.1-5_amd64.buildinfo ./gtk3-nooverlayscrollbar_7.0.1-5_all.deb ./clearlooks-phenix-cinnabar-theme_7.0.1-5_all.deb ./clearlooks-phenix-cinnabar-theme_7.0.1.orig.tar.gz ./clearlooks-phenix-cinnabar-theme_7.0.1-5_amd64.changes
751e178aaab44f45fd21a8d3b7f246f0d3a257383da9e08923df1f7d75df3f22  ./clearlooks-phenix-cinnabar-theme_7.0.1-5.debian.tar.xz
822f0b251edd7d0d107882d104b02f0420ff33723a80110bd290bc5a17c7aeb0  ./buildlog.txt
f3500bb3efb9d37ed45fd2726975e044b67d333695bfa36a2359f4b28d5b471d  ./clearlooks-phenix-cinnabar-theme_7.0.1-5.dsc
df06caea05d62544cdc33217b66c1ffe508aa99f3b554845540510591854b268  ./clearlooks-phenix-cinnabar-theme_7.0.1-5_amd64.buildinfo
518886db2bf9b781fb1a242ec09bc2614f18bc840c16a7000d83e7e43e89438c  ./gtk3-nooverlayscrollbar_7.0.1-5_all.deb
48ec58302297b007be899a757f0d8507ca415c986edfa1497f2765c841303f7d  ./clearlooks-phenix-cinnabar-theme_7.0.1-5_all.deb
25f829755e4d876bddffd889e6e614d170ae5f61b8dcb2ef264602ca90900b3c  ./clearlooks-phenix-cinnabar-theme_7.0.1.orig.tar.gz
0b84e181d4c2547c6f8f9500d5dc1ddfa8d08eb22fb589fd307039cca98d34a2  ./clearlooks-phenix-cinnabar-theme_7.0.1-5_amd64.changes
+ mkdir published
+ cd published
+ cd ../
+ ls clearlooks-phenix-cinnabar-theme_7.0.1-5_all.deb gtk3-nooverlayscrollbar_7.0.1-5_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/c/clearlooks-phenix-cinnabar-theme/clearlooks-phenix-cinnabar-theme_7.0.1-5_all.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/c/clearlooks-phenix-cinnabar-theme/gtk3-nooverlayscrollbar_7.0.1-5_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./gtk3-nooverlayscrollbar_7.0.1-5_all.deb ./clearlooks-phenix-cinnabar-theme_7.0.1-5_all.deb
518886db2bf9b781fb1a242ec09bc2614f18bc840c16a7000d83e7e43e89438c  ./gtk3-nooverlayscrollbar_7.0.1-5_all.deb
48ec58302297b007be899a757f0d8507ca415c986edfa1497f2765c841303f7d  ./clearlooks-phenix-cinnabar-theme_7.0.1-5_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./gtk3-nooverlayscrollbar_7.0.1-5_all.deb: OK
./clearlooks-phenix-cinnabar-theme_7.0.1-5_all.deb: OK
+ echo Package clearlooks-phenix-cinnabar-theme version 7.0.1-5 is reproducible!
Package clearlooks-phenix-cinnabar-theme version 7.0.1-5 is reproducible!
