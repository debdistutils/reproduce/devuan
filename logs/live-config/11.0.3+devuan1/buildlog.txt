+ date
Sun Apr 16 20:42:02 UTC 2023
+ apt-get source --only-source live-config=11.0.3+devuan1
Reading package lists...
NOTICE: 'live-config' packaging is maintained in the 'Git' version control system at:
git@git.devuan.org:devuan/live-config.git
Please use:
git clone git@git.devuan.org:devuan/live-config.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 79.9 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main live-config 11.0.3+devuan1 (dsc) [1456 B]
Get:2 http://deb.devuan.org/merged chimaera/main live-config 11.0.3+devuan1 (tar) [78.5 kB]
dpkg-source: info: extracting live-config in live-config-11.0.3+devuan1
dpkg-source: info: unpacking live-config_11.0.3+devuan1.tar.xz
Fetched 79.9 kB in 2s (41.2 kB/s)
W: Download is performed unsandboxed as root as file 'live-config_11.0.3+devuan1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source live-config=11.0.3+devuan1
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name live-config* -type d
+ cd ./live-config-11.0.3+devuan1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package live-config
dpkg-buildpackage: info: source version 11.0.3+devuan1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean 
   dh_auto_clean
	make -j50 distclean
make[1]: Entering directory '/build/live-config/live-config-11.0.3+devuan1'
Nothing to clean.
Nothing to distclean.
make[1]: Leaving directory '/build/live-config/live-config-11.0.3+devuan1'
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building live-config in live-config_11.0.3+devuan1.tar.xz
dpkg-source: info: building live-config in live-config_11.0.3+devuan1.dsc
 debian/rules build
dh build 
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
   dh_auto_build
	make -j50 "INSTALL=install --strip-program=true"
make[1]: Entering directory '/build/live-config/live-config-11.0.3+devuan1'
Nothing to build.
make[1]: Leaving directory '/build/live-config/live-config-11.0.3+devuan1'
   dh_auto_test
	make -j50 test
make[1]: Entering directory '/build/live-config/live-config-11.0.3+devuan1'
Checking for syntax errors............................................ done.
Checking for bashisms.....................................error: components/1140-xserver-xorg: Unterminated quoted string found, EOF reached. Wanted: <">, opened in line 111
....... done.
make[1]: Leaving directory '/build/live-config/live-config-11.0.3+devuan1'
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary 
   dh_testroot
   dh_prep
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/live-config/live-config-11.0.3+devuan1'
dh_auto_install -- DESTDIR=debian/tmp
	make -j50 install DESTDIR=/build/live-config/live-config-11.0.3\+devuan1/debian/tmp AM_UPDATE_INFO_DIR=no "INSTALL=install --strip-program=true" DESTDIR=debian/tmp
make[2]: Entering directory '/build/live-config/live-config-11.0.3+devuan1'
# Installing backend
mkdir -p debian/tmp/etc/init.d
cp backend/sysvinit/live-config.init debian/tmp/etc/init.d/live-config
# Installing frontend and components
mkdir -p debian/tmp/bin debian/tmp/lib/live/config
cp frontend/live-* debian/tmp/bin
cp frontend/*.sh debian/tmp/lib/live/
cp components/* debian/tmp/lib/live/config
mkdir -p debian/tmp/var/lib/live/config
# Installing shared data
mkdir -p debian/tmp/usr/share/live/config
cp -r VERSION share/* debian/tmp/usr/share/live/config
# Installing docs
mkdir -p debian/tmp/usr/share/doc/live-config
cp -r COPYING examples debian/tmp/usr/share/doc/live-config
# Installing manpages
for MANPAGE in manpages/en/*; \
do \
	SECTION="$(basename ${MANPAGE} | awk -F. '{ print $2 }')"; \
	install -D -m 0644 ${MANPAGE} debian/tmp/usr/share/man/man${SECTION}/$(basename ${MANPAGE}); \
done
for LANGUAGE in es fr it ja; \
do \
	for MANPAGE in manpages/${LANGUAGE}/*; \
	do \
		SECTION="$(basename ${MANPAGE} | awk -F. '{ print $3 }')"; \
		install -D -m 0644 ${MANPAGE} debian/tmp/usr/share/man/${LANGUAGE}/man${SECTION}/$(basename ${MANPAGE} .${LANGUAGE}.${SECTION}).${SECTION}; \
	done; \
done
make[2]: Leaving directory '/build/live-config/live-config-11.0.3+devuan1'
# live-config-sysvinit
mkdir -p debian/live-config-sysvinit/lib/live/config
mv debian/tmp/lib/live/config/*-sysvinit debian/live-config-sysvinit/lib/live/config
mv debian/tmp/lib/live/config/*-sysv-rc debian/live-config-sysvinit/lib/live/config
# live-config-runit
mkdir -p debian/live-config-runit/lib/live/config
mv debian/tmp/lib/live/config/*-runit debian/live-config-runit/lib/live/config
# Using user-setup instead of live-debconfig
rm -f debian/tmp/live/config/*live-debconfig*
# Removing unused files
rm -f debian/tmp/usr/share/doc/live-config/COPYING
frontend/live-config-update debian/tmp
Removing unused components for devuan (chimaera)...
Setting specific defaults for devuan (chimaera)...
make[1]: Leaving directory '/build/live-config/live-config-11.0.3+devuan1'
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_installman
   debian/rules override_dh_installinit
make[1]: Entering directory '/build/live-config/live-config-11.0.3+devuan1'
dh_installinit -p live-config-sysvinit --no-stop-on-upgrade --no-start --update-rcd-params='start 00 S .' --name live-config --onlyscripts
make[1]: Leaving directory '/build/live-config/live-config-11.0.3+devuan1'
   dh_bugfiles
   dh_lintian
   dh_perl
   dh_link
   dh_strip_nondeterminism
   debian/rules override_dh_compress
make[1]: Entering directory '/build/live-config/live-config-11.0.3+devuan1'
dh_compress -Xusr/share/doc/live-config/examples
make[1]: Leaving directory '/build/live-config/live-config-11.0.3+devuan1'
   dh_fixperms
   debian/rules override_dh_missing
make[1]: Entering directory '/build/live-config/live-config-11.0.3+devuan1'
dh_missing --fail-missing
make[1]: Leaving directory '/build/live-config/live-config-11.0.3+devuan1'
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'live-config' in '../live-config_11.0.3+devuan1_all.deb'.
dpkg-deb: building package 'live-config-doc' in '../live-config-doc_11.0.3+devuan1_all.deb'.
dpkg-deb: building package 'live-config-runit' in '../live-config-runit_11.0.3+devuan1_all.deb'.
dpkg-deb: building package 'live-config-sysvinit' in '../live-config-sysvinit_11.0.3+devuan1_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../live-config_11.0.3+devuan1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Apr 16 20:42:26 UTC 2023
+ cd ..
+ ls -la
total 256
drwxr-xr-x  3 root root  4096 Apr 16 20:42 .
drwxr-xr-x  3 root root  4096 Apr 16 20:42 ..
-rw-r--r--  1 root root  7116 Apr 16 20:42 buildlog.txt
drwxr-xr-x 11 root root  4096 Aug 13  2021 live-config-11.0.3+devuan1
-rw-r--r--  1 root root 54248 Apr 16 20:42 live-config-doc_11.0.3+devuan1_all.deb
-rw-r--r--  1 root root 22464 Apr 16 20:42 live-config-runit_11.0.3+devuan1_all.deb
-rw-r--r--  1 root root 23564 Apr 16 20:42 live-config-sysvinit_11.0.3+devuan1_all.deb
-rw-r--r--  1 root root   918 Apr 16 20:42 live-config_11.0.3+devuan1.dsc
-rw-r--r--  1 root root 78364 Apr 16 20:42 live-config_11.0.3+devuan1.tar.xz
-rw-r--r--  1 root root 35256 Apr 16 20:42 live-config_11.0.3+devuan1_all.deb
-rw-r--r--  1 root root  6507 Apr 16 20:42 live-config_11.0.3+devuan1_amd64.buildinfo
-rw-r--r--  1 root root  2880 Apr 16 20:42 live-config_11.0.3+devuan1_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./live-config_11.0.3+devuan1.dsc ./buildlog.txt ./live-config-sysvinit_11.0.3+devuan1_all.deb ./live-config-runit_11.0.3+devuan1_all.deb ./live-config_11.0.3+devuan1_amd64.buildinfo ./live-config-doc_11.0.3+devuan1_all.deb ./live-config_11.0.3+devuan1.tar.xz ./live-config_11.0.3+devuan1_all.deb ./live-config_11.0.3+devuan1_amd64.changes
f96adbbb942eb07bdff8fa7edb1d7dc1d3ff912144f54930a2187f3cebbda6a5  ./live-config_11.0.3+devuan1.dsc
4baffeb34ed613b2d7106d2365bfa5f2dafd8d25c1dd40c8599f482fdf9fe83f  ./buildlog.txt
31e57f4989e99c1a9ab20e2a6f8e3d63a9c685150372842c24a05c35e8ec0b87  ./live-config-sysvinit_11.0.3+devuan1_all.deb
ceb6fb77c07b7a6cf87c19a7d5c346f62c663cbe263ccd93fd1b6624116e8327  ./live-config-runit_11.0.3+devuan1_all.deb
1d61edd061fe338c4f9c3126977b50c2e87c34a808a9025110a72c08fa3b11de  ./live-config_11.0.3+devuan1_amd64.buildinfo
045c068d611867c03cbd704bc80b7168ef6cae3ff5958809f98ed0984ce38855  ./live-config-doc_11.0.3+devuan1_all.deb
0eb3979f071db3e3c77f766f979912fa5a17ca8060695edb1fad38ee845e88da  ./live-config_11.0.3+devuan1.tar.xz
f8a047d21237a8e9a0dd75699df01221084ca15bb34c42e2834b2f45afcc8ab8  ./live-config_11.0.3+devuan1_all.deb
04007f1fce9069a5b8fcc7ea73bd3451195fa6a9cd299d928f4011f073ecbd44  ./live-config_11.0.3+devuan1_amd64.changes
+ mkdir published
+ cd published
+ cd ../
+ ls live-config-doc_11.0.3+devuan1_all.deb live-config-runit_11.0.3+devuan1_all.deb live-config-sysvinit_11.0.3+devuan1_all.deb live-config_11.0.3+devuan1_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/l/live-config/live-config-doc_11.0.3+devuan1_all.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/l/live-config/live-config-runit_11.0.3+devuan1_all.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/l/live-config/live-config-sysvinit_11.0.3+devuan1_all.deb
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/l/live-config/live-config_11.0.3+devuan1_all.deb
+ tee+  ../SHA256SUMSfind
 . -maxdepth 1 -type f
+ sha256sum ./live-config-sysvinit_11.0.3+devuan1_all.deb ./live-config-runit_11.0.3+devuan1_all.deb ./live-config-doc_11.0.3+devuan1_all.deb ./live-config_11.0.3+devuan1_all.deb
31e57f4989e99c1a9ab20e2a6f8e3d63a9c685150372842c24a05c35e8ec0b87  ./live-config-sysvinit_11.0.3+devuan1_all.deb
ceb6fb77c07b7a6cf87c19a7d5c346f62c663cbe263ccd93fd1b6624116e8327  ./live-config-runit_11.0.3+devuan1_all.deb
045c068d611867c03cbd704bc80b7168ef6cae3ff5958809f98ed0984ce38855  ./live-config-doc_11.0.3+devuan1_all.deb
f8a047d21237a8e9a0dd75699df01221084ca15bb34c42e2834b2f45afcc8ab8  ./live-config_11.0.3+devuan1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./live-config-sysvinit_11.0.3+devuan1_all.deb: OK
./live-config-runit_11.0.3+devuan1_all.deb: OK
./live-config-doc_11.0.3+devuan1_all.deb: OK
./live-config_11.0.3+devuan1_all.deb: OK
+ echo Package live-config version 11.0.3+devuan1 is reproducible!
Package live-config version 11.0.3+devuan1 is reproducible!
