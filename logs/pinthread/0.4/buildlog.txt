+ date
Mon Apr 17 06:51:49 UTC 2023
+ apt-get source --only-source pinthread=0.4
Reading package lists...
Need to get 17.3 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main pinthread 0.4 (dsc) [1037 B]
Get:2 http://deb.devuan.org/merged chimaera/main pinthread 0.4 (tar) [16.3 kB]
dpkg-source: info: extracting pinthread in pinthread-0.4
dpkg-source: info: unpacking pinthread_0.4.tar.xz
Fetched 17.3 kB in 0s (68.9 kB/s)
W: Download is performed unsandboxed as root as file 'pinthread_0.4.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source pinthread=0.4
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name pinthread* -type d
+ cd ./pinthread-0.4
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package pinthread
dpkg-buildpackage: info: source version 0.4
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Franco (nextime) Lanza <nextime@nexlab.it>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_clean
dh_auto_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	make -j1 clean
make[1]: Entering directory '/build/pinthread/pinthread-0.4'
rm -f pinthread.so
rm -f dotprod_mutex
make[1]: Leaving directory '/build/pinthread/pinthread-0.4'
   dh_clean
dh_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building pinthread in pinthread_0.4.tar.xz
dpkg-source: info: building pinthread in pinthread_0.4.dsc
 debian/rules build
dh build
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_update_autotools_config
   dh_auto_configure
dh_auto_configure: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_build
dh_auto_build: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	make -j1
make[1]: Entering directory '/build/pinthread/pinthread-0.4'
gcc -Wall -D_GNU_SOURCE -fpic -shared -o pinthread.so pinthread.c -ldl -lpthread
gcc -Wall -lpthread dotprod_mutex.c -o dotprod_mutex
/usr/bin/ld: /tmp/ccyFpTav.o: in function `main':
dotprod_mutex.c:(.text+0x238): undefined reference to `pthread_create'
/usr/bin/ld: dotprod_mutex.c:(.text+0x280): undefined reference to `pthread_join'
collect2: error: ld returned 1 exit status
make[1]: *** [Makefile:3: all] Error 1
make[1]: Leaving directory '/build/pinthread/pinthread-0.4'
dh_auto_build: error: make -j1 returned exit code 2
make: *** [debian/rules:3: build] Error 25
dpkg-buildpackage: error: debian/rules build subprocess returned exit status 2
