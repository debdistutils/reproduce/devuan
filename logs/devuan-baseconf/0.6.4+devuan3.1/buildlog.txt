+ date
Sun Apr 16 19:37:07 UTC 2023
+ apt-get source --only-source devuan-baseconf=0.6.4+devuan3.1
Reading package lists...
NOTICE: 'devuan-baseconf' packaging is maintained in the 'Git' version control system at:
git://github.org/devuan/devuan-baseconf.git
Please use:
git clone git://github.org/devuan/devuan-baseconf.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 16.3 kB of source archives.
Get:1 http://deb.devuan.org/merged chimaera/main devuan-baseconf 0.6.4+devuan3.1 (dsc) [1318 B]
Get:2 http://deb.devuan.org/merged chimaera/main devuan-baseconf 0.6.4+devuan3.1 (tar) [15.0 kB]
dpkg-source: info: extracting devuan-baseconf in devuan-baseconf-0.6.4+devuan3.1
dpkg-source: info: unpacking devuan-baseconf_0.6.4+devuan3.1.tar.xz
Fetched 16.3 kB in 0s (237 kB/s)
W: Download is performed unsandboxed as root as file 'devuan-baseconf_0.6.4+devuan3.1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source devuan-baseconf=0.6.4+devuan3.1
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name devuan-baseconf* -type d
+ cd ./devuan-baseconf-0.6.4+devuan3.1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package devuan-baseconf
dpkg-buildpackage: info: source version 0.6.4+devuan3.1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean 
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building devuan-baseconf in devuan-baseconf_0.6.4+devuan3.1.tar.xz
dpkg-source: info: building devuan-baseconf in devuan-baseconf_0.6.4+devuan3.1.dsc
 debian/rules build
dh build 
   dh_update_autotools_config
   dh_autoreconf
   debian/rules override_dh_auto_build
make[1]: Entering directory '/build/devuan-baseconf/devuan-baseconf-0.6.4+devuan3.1'
sed "s/__DSUITE__/unstable/" debian/postinst > debian/postinst.tmp
mv debian/postinst.tmp debian/postinst
dh_auto_build
make[1]: Leaving directory '/build/devuan-baseconf/devuan-baseconf-0.6.4+devuan3.1'
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary 
   dh_testroot
   dh_prep
   dh_installdirs
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'devuan-baseconf' in '../devuan-baseconf_0.6.4+devuan3.1_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../devuan-baseconf_0.6.4+devuan3.1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Apr 16 19:37:17 UTC 2023
+ cd ..
+ ls -la
total 56
drwxr-xr-x 3 root root  4096 Apr 16 19:37 .
drwxr-xr-x 3 root root  4096 Apr 16 19:37 ..
-rw-r--r-- 1 root root  3143 Apr 16 19:37 buildlog.txt
drwxr-xr-x 4 root root  4096 Jun 13  2019 devuan-baseconf-0.6.4+devuan3.1
-rw-r--r-- 1 root root   780 Apr 16 19:37 devuan-baseconf_0.6.4+devuan3.1.dsc
-rw-r--r-- 1 root root 15020 Apr 16 19:37 devuan-baseconf_0.6.4+devuan3.1.tar.xz
-rw-r--r-- 1 root root  4288 Apr 16 19:37 devuan-baseconf_0.6.4+devuan3.1_all.deb
-rw-r--r-- 1 root root  5140 Apr 16 19:37 devuan-baseconf_0.6.4+devuan3.1_amd64.buildinfo
-rw-r--r-- 1 root root  1838 Apr 16 19:37 devuan-baseconf_0.6.4+devuan3.1_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./devuan-baseconf_0.6.4+devuan3.1_amd64.buildinfo ./devuan-baseconf_0.6.4+devuan3.1.dsc ./devuan-baseconf_0.6.4+devuan3.1.tar.xz ./buildlog.txt ./devuan-baseconf_0.6.4+devuan3.1_all.deb ./devuan-baseconf_0.6.4+devuan3.1_amd64.changes
be0384f0be7f3ca1f539e5e4fc6f7cbb512b8d412f1cc705f6a8b00957c32339  ./devuan-baseconf_0.6.4+devuan3.1_amd64.buildinfo
a4894a391dfa069924096d6448adb49b22f1b067ecb01d3f147de708643d7169  ./devuan-baseconf_0.6.4+devuan3.1.dsc
44621fd9a5ae685efd0b3516a6524c3e248a0254d229c39ce1b484b127904986  ./devuan-baseconf_0.6.4+devuan3.1.tar.xz
369b591d7f3c6d3a13364cb332a15e1a4d086f94ffeacc4e7b6ad9049ebdb089  ./buildlog.txt
62e93fd8016326a21f4ef610ba9afd4e2bf1080b9dc76cba95b88c0de705290d  ./devuan-baseconf_0.6.4+devuan3.1_all.deb
ac250330093e9f2f376a121065d3e54c5d0544df11011dcb6e722bbfc7ff85f9  ./devuan-baseconf_0.6.4+devuan3.1_amd64.changes
+ mkdir published
+ cd published
+ cd ../
+ ls devuan-baseconf_0.6.4+devuan3.1_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://deb.devuan.org/merged/pool/DEVUAN/main/d/devuan-baseconf/devuan-baseconf_0.6.4+devuan3.1_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./devuan-baseconf_0.6.4+devuan3.1_all.deb
62e93fd8016326a21f4ef610ba9afd4e2bf1080b9dc76cba95b88c0de705290d  ./devuan-baseconf_0.6.4+devuan3.1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./devuan-baseconf_0.6.4+devuan3.1_all.deb: OK
+ echo Package devuan-baseconf version 0.6.4+devuan3.1 is reproducible!
Package devuan-baseconf version 0.6.4+devuan3.1 is reproducible!
