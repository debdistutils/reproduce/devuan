# Reproducible builds of Devuan GNU+Linux 4.0 Chimaera

This project provides [reproducible
build](https://reproducible-builds.org/) status for
[Devuan GNU+Linux 4.0 Chimaera](https://www.devuan.org/) on
amd64.

## Status

We have reproducibly built **47%** of the
difference between **Devuan GNU+Linux 4.0 Chimaera** and
**Debian 11 Bullseye**!  That is **47%** of
the packages we have built, and we have built **100%** or
**84** of the **84** source packages to rebuild.

Devuan GNU+Linux 4.0 Chimaera (on amd64) contains binary packages
that were added/modified compared to what is in
Debian 11 Bullseye (on amd64) that corresponds to
**77** source packages.  Some binary packages exists in more
than one version, so there is a total of **84** source packages to
rebuild.  Of these we have built **40**
reproducibly out of the **84** builds so far.

We have build logs for **86** builds, which may exceed the number
of total source packages to rebuild when a particular source package
(or source package version) has been removed from the archive.  Of the
packages we built, **40** packages are reproducible and
there are **37** packages that we could not reproduce.
Building **9** package had build failures.  We do not attempt to
build **0** packages.

[[_TOC_]]

### Unreproducible packages

The following **37** packages unfortunately differ in
some way compared to the version distributed in the archive.  Please
investigate the build log and [diffoscope](https://diffoscope.org)
output and help us fix it!

| Package | Version | Diffoscope output | Link to build log |
| ------- | ------- | ----------------- | ----------------- |
| 389-ds-base | 1.4.4.11-2+devuan0.1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/389-ds-base/1.4.4.11-2+devuan0.1/index.html) | [build log from Sun Apr 16 17:42:07 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/389-ds-base/1.4.4.11-2+devuan0.1/buildlog.txt) |
| apt | 2.2.4+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/apt/2.2.4+devuan1/index.html) | [build log from Sun Apr 16 17:50:27 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/apt/2.2.4+devuan1/buildlog.txt) |
| cgmanager | 0.41-2+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/cgmanager/0.41-2+devuan1/index.html) | [build log from Sun Apr 16 17:50:36 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/cgmanager/0.41-2+devuan1/buildlog.txt) |
| cgroupfs-mount | 1.4+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/cgroupfs-mount/1.4+devuan1/index.html) | [build log from Sun Apr 16 18:12:33 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/cgroupfs-mount/1.4+devuan1/buildlog.txt) |
| colord | 1.4.5-3+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/colord/1.4.5-3+devuan1/index.html) | [build log from Sun Apr 16 19:37:14 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/colord/1.4.5-3+devuan1/buildlog.txt) |
| consolekit2 | 1.2.1-8 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/consolekit2/1.2.1-8/index.html) | [build log from Sun Apr 16 18:09:29 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/consolekit2/1.2.1-8/buildlog.txt) |
| devuan-keyring | 2022.09.04 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/devuan-keyring/2022.09.04/index.html) | [build log from Sun Apr 16 19:47:45 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/devuan-keyring/2022.09.04/buildlog.txt) |
| elogind | 246.10-2 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/elogind/246.10-2/index.html) | [build log from Sun Apr 16 19:59:11 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/elogind/246.10-2/buildlog.txt) |
| eudev | 3.2.9-10~chimaera1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/eudev/3.2.9-10~chimaera1/index.html) | [build log from Sun Apr 16 20:05:15 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/eudev/3.2.9-10~chimaera1/buildlog.txt) |
| hylafax | 3:6.0.7-3.1+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/hylafax/3:6.0.7-3.1+devuan1/index.html) | [build log from Sun Apr 16 20:05:34 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/hylafax/3:6.0.7-3.1+devuan1/buildlog.txt) |
| iwd | 1.14-2+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/iwd/1.14-2+devuan1/index.html) | [build log from Sun Apr 16 20:27:10 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/iwd/1.14-2+devuan1/buildlog.txt) |
| ldm | 2:2.18.06-1+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/ldm/2:2.18.06-1+devuan1/index.html) | [build log from Sun Apr 16 20:15:13 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/ldm/2:2.18.06-1+devuan1/buildlog.txt) |
| libvirt | 7.0.0-3+devuan3 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/libvirt/7.0.0-3+devuan3/index.html) | [build log from Sun Apr 16 20:27:14 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/libvirt/7.0.0-3+devuan3/buildlog.txt) |
| lightdm | 1.26.0-7+devuan3 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/lightdm/1.26.0-7+devuan3/index.html) | [build log from Sun Apr 16 20:15:56 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/lightdm/1.26.0-7+devuan3/buildlog.txt) |
| live-build | 4.0.3-1+devuan2 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/live-build/4.0.3-1+devuan2/index.html) | [build log from Sun Apr 16 20:27:25 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/live-build/4.0.3-1+devuan2/buildlog.txt) |
| ltsp | 5.18.12-3+devuan2 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/ltsp/5.18.12-3+devuan2/index.html) | [build log from Sun Apr 16 20:27:19 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/ltsp/5.18.12-3+devuan2/buildlog.txt) |
| ltspfs | 1.5-2+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/ltspfs/1.5-2+devuan1/index.html) | [build log from Sun Apr 16 20:43:15 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/ltspfs/1.5-2+devuan1/buildlog.txt) |
| net-tools | 1.60+git20181103.0eebece-1+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/net-tools/1.60+git20181103.0eebece-1+devuan1/index.html) | [build log from Mon Apr 17 06:50:04 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/net-tools/1.60+git20181103.0eebece-1+devuan1/buildlog.txt) |
| network-manager | 1.30.6-1+deb11u1devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/network-manager/1.30.6-1+deb11u1devuan1/index.html) | [build log from Sun Apr 16 20:42:07 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/network-manager/1.30.6-1+deb11u1devuan1/buildlog.txt) |
| openvpn | 2.5.1-3+devuan2 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/openvpn/2.5.1-3+devuan2/index.html) | [build log from Mon Apr 17 06:50:12 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/openvpn/2.5.1-3+devuan2/buildlog.txt) |
| packagekit | 1.2.2-2+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/packagekit/1.2.2-2+devuan1/index.html) | [build log from Sun Apr 16 20:42:24 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/packagekit/1.2.2-2+devuan1/buildlog.txt) |
| pcsc-lite | 1.9.1-1+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/pcsc-lite/1.9.1-1+devuan1/index.html) | [build log from Mon Apr 17 07:01:17 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/pcsc-lite/1.9.1-1+devuan1/buildlog.txt) |
| pdns-recursor | 4.4.2-3+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/pdns-recursor/4.4.2-3+devuan1/index.html) | [build log from Mon Apr 17 06:50:41 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/pdns-recursor/4.4.2-3+devuan1/buildlog.txt) |
| plymouth | 0.9.5-2+devuan2 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/plymouth/0.9.5-2+devuan2/index.html) | [build log from Mon Apr 17 07:14:21 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/plymouth/0.9.5-2+devuan2/buildlog.txt) |
| policykit-1 | 0.105-31+devuan2 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/policykit-1/0.105-31+devuan2/index.html) | [build log from Mon Apr 17 07:13:08 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/policykit-1/0.105-31+devuan2/buildlog.txt) |
| popularity-contest | 1.71+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/popularity-contest/1.71+devuan1/index.html) | [build log from Mon Apr 17 07:02:02 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/popularity-contest/1.71+devuan1/buildlog.txt) |
| rrqnet | 1.5.3 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/rrqnet/1.5.3/index.html) | [build log from Mon Apr 17 07:24:30 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/rrqnet/1.5.3/buildlog.txt) |
| slim | 1.3.6-5.2+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/slim/1.3.6-5.2+devuan1/index.html) | [build log from Mon Apr 17 07:34:34 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/slim/1.3.6-5.2+devuan1/buildlog.txt) |
| sysvinit | 3.00-1+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/sysvinit/3.00-1+devuan1/index.html) | [build log from Mon Apr 17 07:46:19 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/sysvinit/3.00-1+devuan1/buildlog.txt) |
| tomcat9 | 9.0.43-3+devuan4 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/tomcat9/9.0.43-3+devuan4/index.html) | [build log from Mon Apr 17 11:09:57 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/tomcat9/9.0.43-3+devuan4/buildlog.txt) |
| tomcat9 | 9.0.43-3+devuan5 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/tomcat9/9.0.43-3+devuan5/index.html) | [build log from Mon Apr 17 08:59:35 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/tomcat9/9.0.43-3+devuan5/buildlog.txt) |
| udev | 1:3.2.9+devuan4 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/udev/1:3.2.9+devuan4/index.html) | [build log from Mon Apr 17 08:15:32 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/udev/1:3.2.9+devuan4/buildlog.txt) |
| udisks2 | 2.9.2-2+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/udisks2/2.9.2-2+devuan1/index.html) | [build log from Mon Apr 17 08:02:53 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/udisks2/2.9.2-2+devuan1/buildlog.txt) |
| util-linux | 2.36.1-8+devuan2 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/util-linux/2.36.1-8+devuan2/index.html) | [build log from Mon Apr 17 08:24:19 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/util-linux/2.36.1-8+devuan2/buildlog.txt) |
| xfce4-session | 4.16.0-1+devuan1 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/xfce4-session/4.16.0-1+devuan1/index.html) | [build log from Mon Apr 17 08:01:14 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/xfce4-session/4.16.0-1+devuan1/buildlog.txt) |
| xlennart | 1.1.1-devuan | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/xlennart/1.1.1-devuan/index.html) | [build log from Mon Apr 17 08:15:03 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/xlennart/1.1.1-devuan/buildlog.txt) |
| xrdp | 0.9.12-1.1+devuan2 | [diffoscope output](https://debdistutils.gitlab.io/reproduce/devuan/diffoscope/xrdp/0.9.12-1.1+devuan2/index.html) | [build log from Mon Apr 17 08:14:50 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/xrdp/0.9.12-1.1+devuan2/buildlog.txt) |

### Build failures

The following **9** packages have build failures, making it
impossible to even compare the binary package in the archive with what
we are able to build locally.

#### Missing source code

The archive is missing source code for the following source packages,
for which the archive is shipping a binary packages that claims it was
built using that particular source package/version.

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |

#### Other build failures

Please investigate the build log and help us fix it!

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
| acdcli | 0.3.2-10 | [build log from Sun Apr 16 17:51:36 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/acdcli/0.3.2-10/buildlog.txt) |
| debian-installer | 20210731+devuan1 | [build log from Sun Apr 16 18:18:25 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/debian-installer/20210731+devuan1/buildlog.txt) |
| debian-installer | 20210731+devuan2 | [build log from Thu May 25 05:54:11 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/debian-installer/20210731+devuan2/buildlog.txt) |
| dnscrypt-proxy | 2.0.45+ds1-1+devuan1 | [build log from Sun Apr 16 19:47:32 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/dnscrypt-proxy/2.0.45+ds1-1+devuan1/buildlog.txt) |
| gpsd | 3.22-4+devuan2 | [build log from Sun Apr 16 20:27:40 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/gpsd/3.22-4+devuan2/buildlog.txt) |
| openjdk-17 | 17.0.7+7-1~deb11u1 | [build log from Fri Jun 16 18:14:47 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/openjdk-17/17.0.7+7-1~deb11u1/buildlog.txt) |
| pinthread | 0.4 | [build log from Mon Apr 17 06:51:49 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/pinthread/0.4/buildlog.txt) |
| rsyslog | 8.2102.0-2+devuan3 | [build log from Mon Apr 17 07:26:30 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/rsyslog/8.2102.0-2+devuan3/buildlog.txt) |
| rsyslog | 8.2302.0-1~bpo11+1devuan2 | [build log from Mon Apr 17 07:34:32 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/rsyslog/8.2302.0-1~bpo11+1devuan2/buildlog.txt) |

### Reproducible packages

The following **40** packages can be built locally to
produce the exact same package that is shipped in the archive.

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
| base-files | 11.1+devuan3 | [build log from Sun Apr 16 18:09:13 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/base-files/11.1+devuan3/buildlog.txt) |
| base-files | 11.1+devuan4 | [build log from Sun May 21 08:38:34 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/base-files/11.1+devuan4/buildlog.txt) |
| cinnabar-icon-theme | 1.1 | [build log from Sun Apr 16 17:50:33 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/cinnabar-icon-theme/1.1/buildlog.txt) |
| clearlooks-phenix-cinnabar-theme | 7.0.1-5 | [build log from Sun Apr 16 18:09:23 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/clearlooks-phenix-cinnabar-theme/7.0.1-5/buildlog.txt) |
| clearlooks-phenix-darkpurpy-theme | 7.0.2-1+devuan3.0 | [build log from Sun Apr 16 18:09:25 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/clearlooks-phenix-darkpurpy-theme/7.0.2-1+devuan3.0/buildlog.txt) |
| clearlooks-phenix-deepsea-theme | 10.0-2 | [build log from Sun Apr 16 18:51:09 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/clearlooks-phenix-deepsea-theme/10.0-2/buildlog.txt) |
| clearlooks-phenix-lightpurpy-theme | 7.0.1-4 | [build log from Sun Apr 16 18:12:35 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/clearlooks-phenix-lightpurpy-theme/7.0.1-4/buildlog.txt) |
| darkpurpy-icon-theme | 1.1 | [build log from Sun Apr 16 18:51:30 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/darkpurpy-icon-theme/1.1/buildlog.txt) |
| dbus | 1.12.24-0+deb11u1devuan1 | [build log from Sun Apr 16 18:12:41 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/dbus/1.12.24-0+deb11u1devuan1/buildlog.txt) |
| debian-config-override | 2.2 | [build log from Sun Apr 16 18:51:16 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/debian-config-override/2.2/buildlog.txt) |
| debootstrap | 1.0.123+devuan3 | [build log from Sun Apr 16 19:37:05 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/debootstrap/1.0.123+devuan3/buildlog.txt) |
| deepsea-icon-theme | 1.1 | [build log from Sun Apr 16 18:51:13 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/deepsea-icon-theme/1.1/buildlog.txt) |
| desktop-base | 1:4.1 | [build log from Sun Apr 16 19:48:54 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/desktop-base/1:4.1/buildlog.txt) |
| devuan-baseconf | 0.6.4+devuan3.1 | [build log from Sun Apr 16 19:37:07 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/devuan-baseconf/0.6.4+devuan3.1/buildlog.txt) |
| devuan-lintian-profile | 1.6 | [build log from Sun Apr 16 20:05:12 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/devuan-lintian-profile/1.6/buildlog.txt) |
| devuan-lintian-profile | 1.7 | [build log from Sun Apr 16 19:37:12 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/devuan-lintian-profile/1.7/buildlog.txt) |
| distro-info-data | 0.51+devuan1 | [build log from Sun Apr 16 19:47:36 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/distro-info-data/0.51+devuan1/buildlog.txt) |
| dq | 20181021-1devuan3 | [build log from Sun Apr 16 20:12:12 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/dq/20181021-1devuan3/buildlog.txt) |
| fontsnaps | 1.4 | [build log from Sun Apr 16 20:12:02 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/fontsnaps/1.4/buildlog.txt) |
| freeipa | 4.8.10-2+devuan3 | [build log from Sun Apr 16 20:05:19 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/freeipa/4.8.10-2+devuan3/buildlog.txt) |
| init-system-helpers | 1.60+devuan1 | [build log from Sun Apr 16 20:05:32 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/init-system-helpers/1.60+devuan1/buildlog.txt) |
| jenkins-buildenv-devuan | 1.2 | [build log from Sun Apr 16 20:12:51 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/jenkins-buildenv-devuan/1.2/buildlog.txt) |
| jenkins-debian-glue-buildenv-devuan | 1.0 | [build log from Sun Apr 16 20:42:15 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/jenkins-debian-glue-buildenv-devuan/1.0/buildlog.txt) |
| live-config | 11.0.3+devuan1 | [build log from Sun Apr 16 20:42:02 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/live-config/11.0.3+devuan1/buildlog.txt) |
| network-manager | 1.42.4-1~bpo11+1devuan1 | [build log from Mon Apr 17 07:01:15 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/network-manager/1.42.4-1~bpo11+1devuan1/buildlog.txt) |
| pam-mkhomedir | 1.0-1 | [build log from Mon Apr 17 06:50:09 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/pam-mkhomedir/1.0-1/buildlog.txt) |
| procps | 2:3.3.17-5+devuan1 | [build log from Mon Apr 17 07:13:40 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/procps/2:3.3.17-5+devuan1/buildlog.txt) |
| refractainstaller-base | 9.6.2 | [build log from Mon Apr 17 07:13:11 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/refractainstaller-base/9.6.2/buildlog.txt) |
| refractainstaller-gui | 9.6.2 | [build log from Mon Apr 17 07:13:33 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/refractainstaller-gui/9.6.2/buildlog.txt) |
| refractasnapshot-base | 10.2.12 | [build log from Mon Apr 17 07:25:54 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/refractasnapshot-base/10.2.12/buildlog.txt) |
| refractasnapshot-gui | 10.2.12 | [build log from Mon Apr 17 07:26:51 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/refractasnapshot-gui/10.2.12/buildlog.txt) |
| reportbug | 7.10.3+devuan1 | [build log from Mon Apr 17 07:25:33 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/reportbug/7.10.3+devuan1/buildlog.txt) |
| seatd | 0.5.0-1 | [build log from Mon Apr 17 07:34:29 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/seatd/0.5.0-1/buildlog.txt) |
| sgm | 0.90.0-2 | [build log from Mon Apr 17 07:47:05 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/sgm/0.90.0-2/buildlog.txt) |
| sshguard | 2.3.1-2+devuan1 | [build log from Mon Apr 17 07:46:04 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/sshguard/2.3.1-2+devuan1/buildlog.txt) |
| sshguard | 2.4.2-1~bpo11+1devuan1 | [build log from Mon Apr 17 07:35:51 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/sshguard/2.4.2-1~bpo11+1devuan1/buildlog.txt) |
| systemctl-service-shim | 0.0.2-1 | [build log from Mon Apr 17 07:34:59 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/systemctl-service-shim/0.0.2-1/buildlog.txt) |
| sysvinit | 2.96-7+devuan2 | [build log from Mon Apr 17 07:44:48 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/sysvinit/2.96-7+devuan2/buildlog.txt) |
| tasksel | 3.68+devuan4u1 | [build log from Mon Apr 17 08:01:05 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/tasksel/3.68+devuan4u1/buildlog.txt) |
| wicd | 1.7.4+tb2-6+devuan2 | [build log from Mon Apr 17 08:01:17 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan/-/blob/main/logs/wicd/1.7.4+tb2-6+devuan2/buildlog.txt) |

### Timestamps

The timestamps of the archives used as input to find out which
packages to reproduce are as follows.  To be precise, these are the
`Date` field in the respectively `Release` file used to construct
the list of packages to evaluate.

When speaking about "current" status of this effort it makes sense to
use the latest timestamp from the set below, which is
**Sun Nov 26 08:40:27 UTC 2023**.

| Suite | Debian 11 Bullseye | Devuan GNU+Linux 4.0 Chimaera |
| ----- | ------------------------------ | -------------------------------- |
| bullseye / chimaera | Sat, 07 Oct 2023 11:07:16 UTC | Sat, 25 Nov 2023 03:24:52 UTC |
| bullseye-updates / chimaera-updates | Sun, 26 Nov 2023 08:13:08 UTC | Sun, 26 Nov 2023 08:40:27 UTC |
| bullseye-security / chimaera-security | Sun, 26 Nov 2023 00:13:30 UTC | Sun, 26 Nov 2023 00:16:15 UTC |
| bullseye-backports / chimaera-backports | Sun, 26 Nov 2023 08:13:09 UTC | Sun, 26 Nov 2023 08:40:08 UTC |

## License

This repository is updated automatically by
[debdistreproduce](https://gitlab.com/debdistutils/debdistreproduce)
but to the extent anything is copyrightable, everything is published
under the AGPLv3+ see the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html].

## Contact

The maintainer of this project is [Simon
Josefsson](https://blog.josefsson.org/).
